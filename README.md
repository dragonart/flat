![flat engine](flat.png)

**Overview**

flat is experimental engine based on hybrid visualization system, that combines:

* **ray tracing** for voxel-based 3D

* oriented bounding boxes **(OBB)** for animations

------------------------------

**Current state**

* **two** separate objects with own OBBs, showing **one** model

* **loading models** from files

* **free camera**, rotating by holding right mouse button and moving by WASD

* **sniper effect** for camera (by changing field of view) using mouse scroll

* **selection sort** for tracing packs

* **~26.139** frames per second

------------------------------

**Planned features**

* different models

* availability to add models and objects in real time (not hardcoding)

* more difficult and deep models

* object selection using mouse

* object moving using mouse or keyboard

* light by calculating normals (I don't know how just now)

* anti-aliasing