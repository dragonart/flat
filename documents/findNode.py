finding intersecting node algorithm:
1. make sorted by tMin list of TracingPack`s -> tracingPacks
2. if tracingPacks is empty, then return node = null
3. nearestIndex = 0, nearestTMin = 0xffffffff
4. for currentIndex from 0 to tracingPacks.size - 1 do
4.1 if tracingPacks[currentIndex].tMin > nearestTMin, then continue
4.2 find intersecting node and update tMin for tracingPacks[currentIndex]
4.3 if tracingPacks[currentIndex].tMin < nearestTMin, then nearestIndex = currentIndex and nearestTMin = tracingPacks[currentIndex].tMin
5. if tracingPacks[nearestIndex].tMin = 0xffffffff, then return node = null
6. return tracingPacks[nearestIndex].node

-----------------------------------------------------------------------------------------------------------------------
TimeLimit = 0xffffffff
TimeDelta = 1.0e-8
-----------------------------------------------------------------------------------------------------------------------

Octree background;

Octree findIntersectingNode(
	Ray ray, 
	Object* objects,
	size_t objectsNumber)
{
	TracingPack tracingPacks[objectsNumber];
	calculateTracingPacks(ray, objects, objectsNumber, tracingPacks);
	sortTracingPacks(tracingPacks, objectsNumber);
	
	if(static_cast<uint32_t>(tracingPacks[0].tMin) == TimeLimit)
		return background;

	size_t nearestIndex = 0;
	float nearestTMin = tracingPacks[0].tMin;

	for(size_t currentIndex = 1; currentIndex < tracingPacks; currentIndex++)
	{
		if(tracingPacks[currentIndex].tMin > nearestTMin)
			continue;
		findIntersectingNode(&ray, &tracingPacks[currentIndex]);

		if(tracingPacks[currentIndex].tMin < nearestTMin)
		{
			nearestTMin = tracingPacks[currentIndex].tMin;
			nearestIndex = currentIndex;
		}
	}

	if(static_cast<uint32_t>(tracingPacks[nearestIndex].tMin) == TimeLimit)
		return background;

	return tracingPacks[nearestIndex].node;
}


-----------------------------------------------------------------------------------------------------------------------

void calculateTracingPacks(
	Ray* ray,
	Object* objects,
	size_t objectsNumber,
	TracingPack* tracingPacks)
{
	for(size_t i = 0; i < objectsNumber; i++)
	{
		Ray transformedRay;
		transformRay(ray, &objects[i].obb, &transformedRay);

		bool intersects;
		float tMax;
		checkThatRayIntersects(&transformedRay.origin, &transformedRay.direction, &objects[i].center, &objects[i].halfSize, &tracingPacks[i].tMin, &tMax, &intersects);

		if(!intersects)
			tracingPacks[i].tMin = static_cast<float>(TimeLimit);
	}
}

-----------------------------------------------------------------------------------------------------------------------

void transformRay(
	Ray* original,
	Obb* obb,
	Ray* result)
{
	result->origin = original->origin - obb->center;
	
	result->direction.x = original->direction * obb->x;
	result->direction.y = original->direction * obb->y;
	result->direction.z = original->direction * obb->z;
}

-----------------------------------------------------------------------------------------------------------------------

void checkThatRayIntersects(
	float3_t* origin,
	float3_t* direction,
	float3_t* center,
	float3_t* halfSize,
	float* tMin,
	float* tMax,
	bool* intersects)
{
	float low;
	float high;

	low = (center->x - halfSize->x - origin->x) / direction->x;
	high = (center->x + halfSize->x - origin->x) / direction->x;

	*tMin = fminf(low, high);
	*tMax = fmaxf(low, high);


	low = (center->y - halfSize->y - origin->y) / direction->y;
	high = (center->y + halfSize->y - origin->y) / direction->y;

	*tMin = fmaxf(*tMin, fminf(low, high));
	*tMax = fminf(*tMax, fmaxf(low, high));


	low = (center->z - halfSize->z - origin->z) / direction->z;
	high = (center->z + halfSize->z - origin->z) / direction->z;

	*tMin = fmaxf(*tMin, fminf(low, high));
	*tMax = fminf(*tMax, fmaxf(low, high));

	if(intersects != nullptr)
		*intersects = (*tMin < *tMax) && (*tMax > 0.0f);
}

-----------------------------------------------------------------------------------------------------------------------

void sortTracingPacks(
	TracingPack* tracingPacks,
	size_t packsNumber)
{
	TracingPack temporary;

	for(size_t i = 0; i < packsNumber; i++)
		for(size_t j = 0; j < packsNumber - 1; j++)
			if(tracingPacks[j].tMin > tracingPacks[j + 1].tMin)
			{
				temporary = tracingPacks[j];
				tracingPacks[j] = tracingPacks[j + 1];
				tracingPacks[j + 1] = temporary;
			}
}

-----------------------------------------------------------------------------------------------------------------------

void findIntersectingNode(
	float3_t rayDirection, 
	TracingPack* tracingPack)
{
	
}

-----------------------------------------------------------------------------------------------------------------------
struct float3_t:
	float x
	float y
	float z
-----------------------------------------------------------------------------------------------------------------------
struct Ray:
	float3_t origin
	float3_t direction
-----------------------------------------------------------------------------------------------------------------------
struct TracingPack:
	Object* object
	float tMin
	Octree node
-----------------------------------------------------------------------------------------------------------------------
struct Object:
	Model* model
	Obb obb
-----------------------------------------------------------------------------------------------------------------------
struct Obb:
	float3_t halfSize
	float3_t center
	float3_t x
	float3_t y
	float3_t z
-----------------------------------------------------------------------------------------------------------------------
struct Model:
	Octree octree
	uint8_t depth
-----------------------------------------------------------------------------------------------------------------------
struct Octree:
	uint32_t color
	uint8_t monolith
	Octree* leaves[8]
-----------------------------------------------------------------------------------------------------------------------

findIntersectingNode(ray, tracingPack):
	if tracingPack.model.root.monolith:
		tracingPack.node = tracingPack.model.root
		return

	target = ray.origin + ray.direction * tracingPack.tMin
	center = [0; 0; 0]
	node = root
	halfSize = tracingPack.obb.halfSize
	stack = []
	index = 0
	level = 1

	while true:
		stack.push([node, center, halfSize])
		halfSize = halfSize/2
		index, center = findContainingNodeIndexAndCenter(center, target, halfSize)
		node = node.leaves[index]
		level = level + 1
		if node != null:
			if node.monolith or level = tracingPack.object.model.depth:
				tracingPack.node = node
				return
			continue
		tMin = 0 
		tMax = 0
		checkThatRayIntersects(&ray.origin, &ray.direction, &center, &halfSize, &tMin, &tMax, null)
		tracingPack.tMin = tMax + TimeDelta
		target = ray.origin + ray.direction * tracingPack.tMin
		if stack is empty:
			tracingPack.tMin = TimeLimit
			tracingPack.node = null
			return
		node, center, halfSize = stack.pop()
		level = level - 1
		contains = checkThatNodeContains(center, target, halfSize)
		while not contains:
			if stack is empty:
				tracingPack.tMin = TimeLimit
				tracingPack.node = null
				return
			node, center, halfSize = stack.pop()
			level = level - 1
			contains = checkThatNodeContains(center, target, halfSize)

	if level = tracingPack.object.model.depth and node != null:
		tracingPack.node = node
		return

	tracingPack.tMin = TimeLimit
	tracingPack.node = null
		
-----------------------------------------------------------------------------------------------------------------------

void findIntersectingNode(
	Ray* ray, 
	TracingPack* tracingPack)
{
	float3_t origin = ray->origin;
	float3_t direction = ray->direction;

	if(tracingPack->tMin < 0.0f)
	{
		origin = origin - direction * tracingPack->tMin;
		tMin = 0.0f;
	}

	if(tracingPack->model->root.monolith)
	{
		tracingPack->node = tracingPack->model->root;
		return;
	}

	float3_t target = origin + direction * tracingPack->tMin;
	float3_t center = float3_t(0.0f, 0.0f, 0.0f);
	tracingPack->node = tracingPack->object->model->root;
	float3_t halfSize = tracingPack->object->obb.halfSize;

	struct TracingStackRecord
	{
		float3_t center;
		Octree* node;
		float3_t halfSize;
	};
	TracingStackRecord stack[tracingPack->object->model->depth];
	uint8_t stackSize = 0;
	
	uint8_t index;
	uint8_t level = 1;

	float tMin, tMax;
	bool contains;

	while(true)
	{
		stack[stackSize++] = TracingStackRecord(node, center, halfSize);
		halfSize /= 2.0f;
		findContainingNodeIndexAndCenter(&center, &target, &halfSize, &index);
		tracingPack->node = tracingPack->node->leaves[index];
		level++;
		if(tracingPack->node != nullptr)
		{
			if(tracingPack->node->monolith || (level == tracingPack->object->model.depth)
				return;
			continue;
		}

		checkThatRayIntersects(&origin, &direction, &center, &halfSize, &tMin, &tMax, nullptr);
		tracingPack->tMin = tMax + TimeDelta;
		target = origin + direction * tracingPack->tMin;
		if(stackSize == 0)
		{
			tracingPack->tMin = TimeLimit;
			tracingPack->node = nullptr;
			return
		}
		tracingPack->node = stack[stackSize - 1].node;
		center = stack[stackSize - 1].center;
		halfSize = stack[stackSize - 1].halfSize;
		stackSize--;
		level--;
		checkThatNodeContains(&center, &target, &halfSize, &contains);
		while(!contains)
		{
			if(stackSize == 0)
			{
				tracingPack->tMin = TimeLimit;
				tracingPack->node = nullptr;
				return;
			}
			tracingPack->node = stack[stackSize - 1].node;
			center = stack[stackSize - 1].center;
			halfSize = stack[stackSize - 1].halfSize;
			stackSize--;
			level--;
			checkThatNodeContains(&center, &target, &halfSize, &contains);
		}
	}

	if((level == tracingPack->object->model->depth) && (tracingPack->node != nullptr)
		return;

	tracingPack->tMin = TimeLimit;
	tracingPack->node = nullptr;
}

-----------------------------------------------------------------------------------------------------------------------                                                |
                        ^ y                     | ray: origin: [-5.; -5.]   tracingPack: object: model: octree: root
                        |                       |   direction: [.45; .89]                      |      |  depth: 3    
		+---+---+---+---+---+---+---+---+       |                                              |   obb: size: 8.  
        |	    | ///// | ///// |       |       |                                              |      center: [0.; 0.]
        +   +   + ///// + ///// +       +       |                                              |           x: [1.; 0.] 
       /|       | ///// | ///// |       |       |                                              |           y: [0.; 1.]
      / +---+---+---+---+-------+---+---+       |                                          tMin: 2.24->3.37      
     /  | ///// | ///// | ///// | ///// |       |                                          node: null
    +   + ///// + ///// + ///// + ///// +       |----------------------------------------------------------------------
        | ///// | ///// | ///// | ///// |       | target: [-4.; -3.]   center: [0.; 0.]     node: root       size: 8.
    +---+---+---+---+---+---+---+---+---+--> x  |         [-3.49; -1.99]       [-2.; -2.]         root[0]          4.
   -5   | ///// | ///// | ///// | ///// |       |                              [-3.; -3.]         null             2.
		+ ///// + ///// + ///// + ///// +       |                              [-2.; -2.]         root[0]          4.
	   /| ///// | ///// | ///// | ///// |       |                              [-3.; -1.]         root[0][2]       2.
	  / +---+---+---+---+---+---+---+---+       |                      
	 /  |       | ///// | ///// |       |       | index: 0              level: 1  
    +   +   +   + ///// + ///// +       +       |        0                     2                                     
	   /|       | ///// | ///// |       |       |        0                     3                                      
	  /	+---+---+---+---+---+---+---+---+       |        2                     2                                    
     /                  |                       |                              3                                    
    +                   +-5                     | stack: root|[0.; 0.]|8.                                     
                                                |        root|[0.; 0.]|8. root[0]|[-2.; -2.]|4. 
                                                |        root|[0.; 0.]|8.
                                                |        root|[0.; 0.]|8. root[0][2]|[-3.; -1.]|2.
                                                |
                                                |
-----------------------------------------------------------------------------------------------------------------------

bool checkThatNodeContains(center, target, halfSize):
	if abs(center.x - target.x) > halfSize.x:
		return false
	if abs(center.y - target.y) > halfSize.y:
		return false
	if abs(center.z - target.z) > halfSize.z:
		return false
	return true

-----------------------------------------------------------------------------------------------------------------------

void checkThatNodeContains(
	float3_t* center,
	float3_t* target,
	float3_t* halfSize,
	bool* contains)
{
	if(abs(center->x - target->x) > halfSize->x)
	{
		*contains = false;
		return;
	}
	if(abs(center->y - target->y) > halfSize->y)
	{
		*contains = false;
		return;
	}
	if(abs(center->z - target->z) > halfSize->z)
	{
		*contains = false;
		return;
	}

	*contains = true;
}

-----------------------------------------------------------------------------------------------------------------------

uint8, float3 findContainingNodeIndexAndCenter(center, target, quaterSize):
	index = 0
	x = center.x - quaterSize.x
	y = center.y - quaterSize.y
	z = center.z - quaterSize.z

	if target.x > center.x:
		index = 1
		x = x + 2 * quaterSize.x

	if target.y > center.y:
		index = index + 2
		y = y + 2 * quaterSize.y

	if target.z > center.z:
		index = index + 4
		z = z + 2 * quaterSize.z

	return index, [x; y; z]

-----------------------------------------------------------------------------------------------------------------------

void findContainingNodeIndexAndCenter(
	float3_t* center,
	float3_t* target,
	float3_t* quaterSize,
	uint8_t* index)
{
	*index = 0;

	float3_t subCenter;
	subCenter->x = center->x - quaterSize->x;
	subCenter->y = center->y - quaterSize->y;
	subCenter->z = center->z - quaterSize->z;

	float halfSize = 2 * quaterSize;

	if(target->x > center->x)
	{
		*index = 1;
		subCenter->x += halfSize->x;
	}

	if(target->y > center->y)
	{
		*index += 2;
		subCenter->y += halfSize->y;
	}

	if(target->z > center->z)
	{
		*index += 4;
		subCenter->z += halfSize->z;
	}

	*center = subCenter;
}

-----------------------------------------------------------------------------------------------------------------------

findIntersectingNode(ray, tracingPack): # old non-working version
	if root.monolith:
		return root

	target = ray.origin + ray.direction * tMin
	center = [0; 0; 0]
	found = false
	node = root
	stack = []
	size = tracingPack.obb.size

	while true:
		index, subCenter = findContainingNodeIndexAndCenter(center, target, size/4)
		leave = node.leaves[index]
		if leave != nullptr:
			if leave.monolith:
				return leave
		
			stack.push([node, center, size])
			node = leave
			center = subCenter
			continue
		
		node, center, size = stack.pop()

-----------------------------------------------------------------------------------------------------------------------

findIntersectingNode(ray, tracingPack): # working, but not for all tests version
	if tracingPack.model.root.monolith:
		tracingPack.node = tracingPack.model.root
		return

	target = ray.origin + ray.direction * tracingPack.tMin
	center = [0; 0; 0]
	node = root
	size = tracingPack.obb.size
	stack = []
	stack.push([tracingPack.model.root, center, size])
	index = 0
	subCenter = [0; 0; 0]
	leave = root
	level = 1

	while stack is not empty and level < tracingPack.object.model.depth:
		index, subCenter = findContainingNodeIndexAndCenter(center, target, size/4)
		leave = node.leaves[index]
		level = level + 1
		size = size/2
		if leave != null:
			if leave.monolith:
				tracingPack.node = leave
				return
			node = leave
			center = subCenter
			stack.push([node, center, size])
			continue
		tMin = 0 
		tMax = 0
		checkThatRayIntersects(&ray.origin, &ray.direction, &subCenter, size/2, &tMin, &tMax, null)
		tracingPack.tMin = tMax + TimeDelta
		target = ray.origin + ray.direction * tracingPack.tMin
		node, center, size = stack.pop()
		level = level - 1

	if level = tracingPack.object.model.depth and node != null:
		tracingPack.node = node
		return

	tracingPack.tMin = TimeLimit
	tracingPack.node = null

-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------