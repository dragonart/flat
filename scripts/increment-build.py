import sys


version_file_name = sys.argv[1]
builds = 666
text = []
for line in open(version_file_name):
	if line == "\n":
		text.append("")
	else:
		if len(line) > 1:
			words = line.split(" ")
			for i in range(0, len(words)):
				if words[i] == "Build":
					builds = (int)((words[i + 2])[:-2])
					print("last build number: " + str(builds))
					builds = builds + 1
					for j in range(0, len(line)):
						if line[j] == "=":
							line = line[:j + 2] + str(builds) + ";\n"
							break
					break
			text.append(line[:-1])
		else:
			text.append(line)

file = open(version_file_name, "w")
for line in text:
	file.write(line + "\n")