#include "demo.h"


int main(int argc,
         char** argv)
{
    flat::demo::Demo demo;

    return demo.main(argc, argv);
}