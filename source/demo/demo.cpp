#include <iostream>

#define GLEW_STATIC
#include <GL/glew.h>

#include "flat/core/version.h"
#include "gpgpu/gpgpu-provider.h"
#include "input/input-provider.h"
#include "input/keyboard-key-event.h"
#include "input/mouse-button-event.h"
#include "input/mouse-move-event.h"
#include "input/mouse-scroll-event.h"
#include "input/window-event.h"

#include "demo.h"


using namespace flat;
using namespace flat::demo;

using std::cout;
using std::endl;
using std::logic_error;
using std::unique_lock;
using std::static_pointer_cast;


void Demo::initialize(int argc,
                      char** argv)
{
    Size size(512, 512);

    initializeWindows(size);
    initializeGraphics();

    _gpgpu = GpgpuProvider::gpgpu();
    _gpgpu->initialize();
    _gpgpu->reserve(10240000);
    _input = InputProvider::input();
    _input->initialize(_window);
    _world.initialize();
    _screen.initialize(size);

    _not_stop = true;

    cout << "Initialization succeeded." << endl;
}

void Demo::release()
{
    _screen.release();
    _world.release();
    _input->release();
    _gpgpu->release();
    glfwDestroyWindow(_window);

    cout << "Release succeeded." << endl;
}

void Demo::update()
{
    double elapsed;

    processInputEvents();

    _input->applyEvents();
    processInputStates();

    _world.update();

    elapsed = _world.prepareFrame(_screen.getFrame());
    _screen.render();

    glfwSwapBuffers(_window);
    glfwPollEvents();

    cout << elapsed << " seconds." << endl;
}


void Demo::processException(exception& e)
{
    cout << "Fail: " << e.what() << endl;
}

void Demo::initializeWindows(const Size& size)
{
    int glfw_major, glfw_minor, glfw_revision;
    glfwGetVersion(&glfw_major, &glfw_minor, &glfw_revision);
    cout << "Using GLFW v." << glfw_major << "." << glfw_minor << "." << glfw_revision << "." << endl;

    if (!glfwInit())
        throw std::runtime_error("GLFW initialization failed. Further windows initialization is impossible.");

    glfwWindowHint(GLFW_SAMPLES, 1);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    auto error_processor = [](int code, const char* message)
    {
        cout << "GLFW fail (code: " << code << "): " << message << endl;
    };
    glfwSetErrorCallback(error_processor);


    const string title = "flat engine v." + Version::getLine() + " - demo";
    _window = glfwCreateWindow(size.width, size.height, title.c_str(), nullptr, nullptr);

    if (!_window)
    {
        glfwTerminate();
        throw std::runtime_error("Failed to open GLFW window.\n" \
                                 "Windows initialization failed.");
    }

    glfwSetWindowPos(_window, 50, 50);
    glfwMakeContextCurrent(_window);

    cout << "Windows initialization succeeded." << endl;
}

void Demo::initializeGraphics()
{
    glewExperimental = true;

    if (glewInit() != GLEW_OK)
        throw std::runtime_error("Failed to initialize GLEW.\n" \
                                 "Graphics initialization failed.");

    cout << "Graphics initialization succeeded." << endl;
}

void Demo::processInputEvents()
{
    auto events = _input->events();
    for (auto& event : *events)
    {
        if (event->group() == InputEvent::Group::Keyboard)
        {
            auto keyboard_event = static_pointer_cast<KeyboardEvent>(event);

            if (keyboard_event->type() == KeyboardEvent::Type::Key)
            {
                auto key_event = static_pointer_cast<KeyboardKeyEvent>(keyboard_event);

                if ((key_event->key() == KeyboardState::KeyCode::Escape) && (key_event->action() == ButtonAction::Press))
                {
                    cout << "You are trying to stop this sodomy by pressing ESC. Ha-ha. OK." << endl;
                    _not_stop = false;
                }
            }

            continue;
        }

        if (event->group() == InputEvent::Group::Mouse)
        {
            auto mouse_event = static_pointer_cast<MouseEvent>(event);

            if (mouse_event->type() == MouseEvent::Type::Move)
            {
                auto move_event = static_pointer_cast<MouseMoveEvent>(mouse_event);

                if (_input->mouse().button(MouseState::ButtonCode::Right) == ButtonState::Pressed)
                {
                    if (move_event->deltaX() > 0.0)
                        _world.execute(CommandConstructor::construct("camera", "rotate", "right"));
                    if (move_event->deltaX() < 0.0)
                        _world.execute(CommandConstructor::construct("camera", "rotate", "left"));

                    if (move_event->deltaY() > 0.0)
                        _world.execute(CommandConstructor::construct("camera", "rotate", "down"));
                    if (move_event->deltaY() < 0.0)
                        _world.execute(CommandConstructor::construct("camera", "rotate", "up"));
                }

                continue;
            }

            if(mouse_event->type() == MouseEvent::Type::Scroll)
            {
                auto scroll_event = static_pointer_cast<MouseScrollEvent>(mouse_event);

                if(scroll_event->offset_y() > 0.0)
                    _world.execute(CommandConstructor::construct("camera", "zoom", "in"));
                if(scroll_event->offset_y() < 0.0)
                    _world.execute(CommandConstructor::construct("camera", "zoom", "out"));

                continue;
            }

            continue;
        }

        if(event->group() == InputEvent::Group::Window)
        {
            auto window_event = static_pointer_cast<WindowEvent>(event);

            if(window_event->type() == WindowEvent::Type::Close)
            {
                cout << "You are trying to stop this sodomy by closing window. Ha-ha. OK." << endl;
                _not_stop = false;
            }
        }
    }
}

void Demo::processInputStates()
{
    auto& keyboard = _input->keyboard();

    if (keyboard.key(KeyboardState::KeyCode::W) == ButtonState::Pressed)
        _world.execute(CommandConstructor::construct("camera", "move", "forwards"));
    if (keyboard.key(KeyboardState::KeyCode::S) == ButtonState::Pressed)
        _world.execute(CommandConstructor::construct("camera", "move", "backwards"));
    if (keyboard.key(KeyboardState::KeyCode::A) == ButtonState::Pressed)
        _world.execute(CommandConstructor::construct("camera", "move", "left"));
    if (keyboard.key(KeyboardState::KeyCode::D) == ButtonState::Pressed)
        _world.execute(CommandConstructor::construct("camera", "move", "right"));

    if (keyboard.key(KeyboardState::KeyCode::ArrowUp) == ButtonState::Pressed)
        _world.execute(CommandConstructor::construct("camera", "rotate", "up"));
    if (keyboard.key(KeyboardState::KeyCode::ArrowDown) == ButtonState::Pressed)
        _world.execute(CommandConstructor::construct("camera", "rotate", "down"));
    if (keyboard.key(KeyboardState::KeyCode::ArrowLeft) == ButtonState::Pressed)
        _world.execute(CommandConstructor::construct("camera", "rotate", "left"));
    if (keyboard.key(KeyboardState::KeyCode::ArrowRight) == ButtonState::Pressed)
        _world.execute(CommandConstructor::construct("camera", "rotate", "right"));
}
