#pragma once


#include <GLFW/glfw3.h>

#include "flat/core/core.h"
#include "flat/command/command-constructor.h"
#include "flat/gpgpu/gpgpu.h"
#include "flat/graphics/screen.h"
#include "flat/input/input.h"
#include "flat/world/world.h"


namespace flat
{
    namespace demo
    {
        class Demo : public Core
        {
        private:
            void initialize(int argc,
                            char** argv) override;
            void release() override;

            void update() override;
            void processException(exception& e) override;

            void processInputEvents();
            void processInputStates();

            void initializeWindows(const Size& size);
            void initializeGraphics();


        private:
            GLFWwindow* _window;
            shared_ptr<Input> _input;
            Screen _screen;
            World _world;
            shared_ptr<Gpgpu> _gpgpu;

        };
    }
}