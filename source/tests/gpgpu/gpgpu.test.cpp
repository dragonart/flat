#include <catch.hpp>

#include "flat/gpgpu/gpgpu.h"


using namespace flat;


TEST_CASE("Gpgpu: initialize() and release()", "[Gpgpu]")
{
    Gpgpu gpgpu;
    REQUIRE_NOTHROW(gpgpu.initialize());
    REQUIRE_NOTHROW(gpgpu.release());
}

TEST_CASE("Gpgpu: createBuffer() empty", "[Gpgpu, Buffer]")
{
    Gpgpu gpgpu;
    gpgpu.initialize();
    gpgpu.reserve(8);

    shared_ptr<Buffer> buffer;
    REQUIRE_NOTHROW(buffer = gpgpu.createBuffer(nullptr, 8));

    gpgpu.release();
}

TEST_CASE("Gpgpu: createBuffer() with value", "[Gpgpu, Buffer]")
{
    Gpgpu gpgpu;
    gpgpu.initialize();
    gpgpu.reserve(8);

    char value[8] = {0, 1, 2, 3, 4, 5, 6, 7};

    shared_ptr<Buffer> buffer;
    REQUIRE_NOTHROW(buffer = gpgpu.createBuffer(value, 8));

    gpgpu.release();
}

TEST_CASE("Gpgpu: createProgram() with one kernel only", "[Gpgpu, Program]")
{
    Gpgpu gpgpu;
    gpgpu.initialize();

    string source =
            "__kernel void foo(float3 argument)\n" \
            "{\n" \
            "   const uint x = get_global_id(0);\n" \
            "   const uint y = get_global_id(1);\n" \
            "   if (argument.x > (float)x) return;\n" \
            "   if (argument.y > (float)y) return;\n" \
            "   if (argument.z > 0.0f) return;\n" \
            "}";

    shared_ptr<Program> program;
    REQUIRE_NOTHROW(program = gpgpu.createProgram(source));

    gpgpu.release();
}

TEST_CASE("Gpgpu: createProgram() with user defined type", "[Gpgpu, Program]")
{
    Gpgpu gpgpu;
    gpgpu.initialize();

    string source =
            "struct Color\n" \
            "{\n" \
            "   uchar red, green, blue, alpha;\n" \
            "};\n" \
            "\n" \
            "__kernel void calculateFrame(__global struct Color* frame,\n" \
            "                             uint width)\n" \
            "{\n" \
            "   const uint x = get_global_id(0);\n" \
            "   const uint y = get_global_id(1);\n" \
            "   frame[y * width + x].red = x % 256;\n" \
            "   frame[y * width + x].green = y % 256;\n" \
            "}";

    shared_ptr<Program> program;
    REQUIRE_NOTHROW(program = gpgpu.createProgram(source));

    gpgpu.release();
}

TEST_CASE("Gpgpu: createKernel()", "[Gpgpu, Program, Kernel]")
{
    Gpgpu gpgpu;
    gpgpu.initialize();

    string source =
            "struct Color\n" \
            "{\n" \
            "   uchar red, green, blue, alpha;\n" \
            "};\n" \
            "\n" \
            "__kernel void calculateFrame(__global struct Color* frame,\n" \
            "                             uint width)\n" \
            "{\n" \
            "   const uint x = get_global_id(0);\n" \
            "   const uint y = get_global_id(1);\n" \
            "   frame[y * width + x].red = x % 256;\n" \
            "   frame[y * width + x].green = y % 256;\n" \
            "}";

    shared_ptr<Program> program;
    REQUIRE_NOTHROW(program = gpgpu.createProgram(source));

    shared_ptr<Kernel> kernel;
    REQUIRE_NOTHROW(kernel = gpgpu.createKernel(program, "calculateFrame"));

    gpgpu.release();
}

TEST_CASE("Gpgpu: runKernel() without arguments", "[Gpgpu, Program, Kernel]")
{
    Gpgpu gpgpu;
    gpgpu.initialize();

    string source =
            "__kernel void fillArray()\n" \
            "{\n" \
            "   const uint x = get_global_id(0);\n" \
            "}";

    shared_ptr<Program> program;
    REQUIRE_NOTHROW(program = gpgpu.createProgram(source));

    shared_ptr<Kernel> kernel;
    REQUIRE_NOTHROW(kernel = gpgpu.createKernel(program, "fillArray"));

    size_t size[] = {5, 1, 1};
    REQUIRE_NOTHROW(gpgpu.runKernel(kernel, 1, size));

    size[1] = 3;
    REQUIRE_NOTHROW(gpgpu.runKernel(kernel, 2, size));

    size[2] = 50;
    REQUIRE_NOTHROW(gpgpu.runKernel(kernel, 3, size));

    gpgpu.release();
}

TEST_CASE("Gpgpu: runKernel() after setKernelArgument() with simple value from pointer", "[Gpgpu, Program, Kernel]")
{
    Gpgpu gpgpu;
    gpgpu.initialize();

    string source =
            "__kernel void fillArray(uint z)\n" \
            "{\n" \
            "   const uint x = get_global_id(0);\n" \
            "   ulong y = x * z;" \
            "}";

    shared_ptr<Program> program;
    REQUIRE_NOTHROW(program = gpgpu.createProgram(source));

    shared_ptr<Kernel> kernel;
    REQUIRE_NOTHROW(kernel = gpgpu.createKernel(program, "fillArray"));

    unsigned int z = 42;
    REQUIRE_NOTHROW(gpgpu.setKernelArgument(kernel, 0, &z, sizeof(unsigned int)));

    size_t size[] = {5, 1, 1};
    REQUIRE_NOTHROW(gpgpu.runKernel(kernel, 1, size));

    gpgpu.release();
}

TEST_CASE("Gpgpu: readBuffer() after runKernel() after setKernelArgument() with Buffer", "[Gpgpu, Program, Kernel, Buffer]")
{
    Gpgpu gpgpu;
    gpgpu.initialize();
    gpgpu.reserve(sizeof(unsigned int) * 5);

    string source =
            "struct Buffer { ulong start; ulong size; };\n" \
            "__kernel void fillArray(struct Buffer array,\n" \
            "                        __global uchar* memory)\n" \
            "{\n" \
            "   const uint x = get_global_id(0);\n" \
            "   __global uint* array_ptr = &memory[array.start];\n" \
            "   array_ptr[x] = x*x;\n" \
            "}";

    shared_ptr<Program> program;
    REQUIRE_NOTHROW(program = gpgpu.createProgram(source));

    shared_ptr<Kernel> kernel;
    REQUIRE_NOTHROW(kernel = gpgpu.createKernel(program, "fillArray"));

    auto array_buffer = gpgpu.createBuffer(nullptr, sizeof(unsigned int) * 5);
    REQUIRE_NOTHROW(gpgpu.setKernelArgument(kernel, 0, array_buffer));

    size_t size[] = {5, 1, 1};
    REQUIRE_NOTHROW(gpgpu.runKernel(kernel, 1, size));

    unsigned int array[5];
    REQUIRE_NOTHROW(gpgpu.readBuffer(array_buffer, array));

    for (unsigned int i = 0; i < 5; ++i)
        REQUIRE(array[i] == i * i);

    gpgpu.release();
}

TEST_CASE("Gpgpu: readBuffer() after runKernel() after setKernelArgument() with Buffer with structure, that stores array with fixed size", "[Gpgpu, Program, Kernel, Buffer]")
{
    Gpgpu gpgpu;
    gpgpu.initialize();
    gpgpu.reserve(5);

    string source =
            "struct Object\n" \
            "{\n" \
            "   uchar array[5];\n" \
            "};\n" \
            "struct Buffer { ulong start; ulong size; };\n" \
            "__kernel void fillArray(struct Buffer object,\n" \
            "                        __global uchar* memory)\n" \
            "{\n" \
            "   const uint x = get_global_id(0);\n" \
            "   __global struct Object* object_ptr = &memory[object.start];\n" \
            "   object_ptr->array[x] = x*x;\n" \
            "}";

    shared_ptr<Program> program;
    REQUIRE_NOTHROW(program = gpgpu.createProgram(source));

    shared_ptr<Kernel> kernel;
    REQUIRE_NOTHROW(kernel = gpgpu.createKernel(program, "fillArray"));

    struct Object
    {
        unsigned char array[5];
    } object;

    auto object_buffer = gpgpu.createBuffer(&object, sizeof(Object));

    REQUIRE_NOTHROW(gpgpu.setKernelArgument(kernel, 0, object_buffer));

    size_t size[] = {5, 1, 1};
    REQUIRE_NOTHROW(gpgpu.runKernel(kernel, 1, size));

    REQUIRE_NOTHROW(gpgpu.readBuffer(object_buffer, &object));

    for (unsigned char i = 0; i < 5; ++i)
        REQUIRE(object.array[i] == i * i);

    gpgpu.release();
}