#include <catch.hpp>

#include "flat/math/vector.h"


using namespace flat;


TEST_CASE("Vector: default constructor", "[Vector]")
{
    Vector v;
    REQUIRE(v.x == 0.0f);
    REQUIRE(v.y == 0.0f);
    REQUIRE(v.z == 0.0f);
}

TEST_CASE("Vector: constructor from 3 floats", "[Vector]")
{
    Vector w(1.0f, 2.0f, 3.0f);
    REQUIRE(w.x == 1.0f);
    REQUIRE(w.y == 2.0f);
    REQUIRE(w.z == 3.0f);
}

TEST_CASE("Vector: constructor from Vector", "[Vector]")
{
    Vector w(1.0f, 2.0f, 3.0f);

    Vector v(w);
    REQUIRE(v.x == 1.0f);
    REQUIRE(v.y == 2.0f);
    REQUIRE(v.z == 3.0f);
}

TEST_CASE("Vector: operator = (Vector)", "[Vector]")
{
    Vector v(1.0f, 2.0f, 3.0f);

    auto w = v;
    REQUIRE(w.x == 1.0f);
    REQUIRE(w.y == 2.0f);
    REQUIRE(w.z == 3.0f);
}


TEST_CASE("Vector: operator == (Vector)", "[Vector]")
{
    Vector v(1.0f, 2.0f, 3.0f);
    Vector w(1.0f, 2.0f, 3.0f);

    REQUIRE(v == w);
}

TEST_CASE("Vector: operator != (Vector)", "[Vector]")
{
    Vector v(1.0f, 2.0f, 3.0f);
    Vector w(1.5f, 2.0f, 3.0f);

    REQUIRE(v != w);
}


TEST_CASE("Vector: operator + (Vector)", "[Vector]")
{
    Vector v1(1.0f, 2.0f, 3.0f);
    Vector v2(4.5f, 5.5f, 6.5f);

    auto w = v1 + v2;
    REQUIRE(w.x == Approx(5.5f));
    REQUIRE(w.y == Approx(7.5f));
    REQUIRE(w.z == Approx(9.5f));
}

TEST_CASE("Vector: operator += (Vector)", "[Vector]")
{
    Vector v(1.0f, 2.0f, 3.0f);
    Vector w(4.5f, 5.5f, 6.5f);

    v += w;
    REQUIRE(v.x == Approx(5.5f));
    REQUIRE(v.y == Approx(7.5f));
    REQUIRE(v.z == Approx(9.5f));
}

TEST_CASE("Vector: operator - (Vector)", "[Vector]")
{
    Vector v1(1.0f, 2.0f, 3.0f);
    Vector v2(4.5f, 5.3f, 6.2f);

    auto w = v2 - v1;
    REQUIRE(w.x == Approx(3.5f));
    REQUIRE(w.y == Approx(3.3f));
    REQUIRE(w.z == Approx(3.2f));
}

TEST_CASE("Vector: operator -= (Vector)", "[Vector]")
{
    Vector v(1.0f, 2.0f, 3.0f);
    Vector w(4.5f, 5.5f, 6.5f);

    v -= w;
    REQUIRE(v.x == Approx(-3.5f));
    REQUIRE(v.y == Approx(-3.5f));
    REQUIRE(v.z == Approx(-3.5f));
}

TEST_CASE("Vector: operator * (float)", "[Vector]")
{
    Vector v(1.0f, 2.0f, 3.0f);

    Vector w = v * 2.5f;
    REQUIRE(w.x == Approx(2.5f));
    REQUIRE(w.y == Approx(5.0f));
    REQUIRE(w.z == Approx(7.5f));
}

TEST_CASE("Vector: operator *= (float)", "[Vector]")
{
    Vector v(1.0f, 2.0f, 3.0f);

    v *= 2.5f;
    REQUIRE(v.x == Approx(2.5f));
    REQUIRE(v.y == Approx(5.0f));
    REQUIRE(v.z == Approx(7.5f));
}

TEST_CASE("Vector: operator / (float)", "[Vector]")
{
    Vector v(2.5f, 5.0f, 7.5f);

    auto w = v / 2.5f;
    REQUIRE(w.x == Approx(1.0f));
    REQUIRE(w.y == Approx(2.0f));
    REQUIRE(w.z == Approx(3.0f));
}

TEST_CASE("Vector: operator /= (float)", "[Vector]")
{
    Vector v(2.5f, 5.0f, 7.5f);

    v /= 2.5f;
    REQUIRE(v.x == Approx(1.0f));
    REQUIRE(v.y == Approx(2.0f));
    REQUIRE(v.z == Approx(3.0f));
}


TEST_CASE("Vector: normalize()", "[Vector]")
{
    Vector v(3.1f, 4.87f, 9.23f);
    v = Vector::normalize(v);

    REQUIRE(Vector::length(v) == Approx(1.0f));
}

TEST_CASE("Vector: conjugate()", "[Vector]")
{
    Vector v(1.5f, 2.5f, 5.5f);

    auto w = Vector::conjugate(v);
    REQUIRE(w.x == Approx(-1.5f));
    REQUIRE(w.y == Approx(-2.5f));
    REQUIRE(w.z == Approx(-5.5f));
}

TEST_CASE("Vector: inverse()", "[Vector]")
{
    Vector v(1.0f, 2.0f, 3.0f);

    auto w = Vector::inverse(v);
    REQUIRE(w.x == Approx(-0.26726f));
    REQUIRE(w.y == Approx(-0.53452248f));
    REQUIRE(w.z == Approx(-0.80178f));
}


TEST_CASE("Vector: norm()", "[Vector]")
{
    Vector v(1.0f, 2.0f, 3.0f);

    REQUIRE(Vector::norm(v) == Approx(14.0f));
}

TEST_CASE("Vector: length()", "[Vector]")
{
    Vector v(1.0f, 2.0f, 3.0f);

    REQUIRE(Vector::length(v) == Approx(3.741657f));
}


TEST_CASE("Vector: dot()", "[Vector]")
{
    Vector v(1.5f, 2.5f, 5.5f);
    Vector w(4.0f, 2.0f, 9.0f);

    REQUIRE(Vector::dot(v, w) == Approx(60.5f));
}

TEST_CASE("Vector: cross()", "[Vector]")
{
    Vector x(1.0f, 2.0f, 3.0f);
    Vector y(5.0f, 6.0f, 7.0f);

    auto z = Vector::cross(x, y);
    REQUIRE(z.x == Approx(-4.0f));
    REQUIRE(z.y == Approx(8.0f));
    REQUIRE(z.z == Approx(-4.0f));
}

TEST_CASE("Vector: size()", "[Vector]")
{
    Vector v;

    REQUIRE(v.size(Serializable::Type::MinimalSize) == sizeof(float) * 3);
    REQUIRE(v.size(Serializable::Type::WithAlignment) == sizeof(float) * 4);

    REQUIRE_THROWS(v.size(Serializable::Type::Unknown));
}

TEST_CASE("Vector: serialize() / deserialize() - with vector", "[Vector]")
{
    Vector v(1.1f, 2.2f, 3.3f);

    auto data_for_network = v.serialize(Serializable::Type::MinimalSize);
    REQUIRE(data_for_network.size() == sizeof(float) * 3);

    Vector deserialized_from_network;
    deserialized_from_network.deserialize(data_for_network, Serializable::Type::MinimalSize);
    REQUIRE(deserialized_from_network == v);

    auto data_for_gppgu = v.serialize(Serializable::Type::WithAlignment);
    REQUIRE(data_for_gppgu.size() == sizeof(float) * 4);

    Vector deserialized_from_gpgpu;
    deserialized_from_gpgpu.deserialize(data_for_gppgu, Serializable::Type::WithAlignment);
    REQUIRE(deserialized_from_gpgpu == v);
}

TEST_CASE("Vector: serialize() / deserialize() - with pointer", "[Vector]")
{
    Vector v(1.1f, 2.2f, 3.3f);

    vector<unsigned char> data_for_network(v.size(Serializable::Type::MinimalSize));
    v.serialize(data_for_network.data(), Serializable::Type::MinimalSize);

    Vector deserialized_from_network;
    deserialized_from_network.deserialize(data_for_network.data(), Serializable::Type::MinimalSize);
    REQUIRE(deserialized_from_network == v);

    vector<unsigned char> data_for_gpgpu(v.size(Serializable::Type::WithAlignment));
    v.serialize(data_for_gpgpu.data(), Serializable::Type::WithAlignment);

    Vector deserialized_from_gpgpu;
    deserialized_from_gpgpu.deserialize(data_for_gpgpu.data(), Serializable::Type::WithAlignment);
    REQUIRE(deserialized_from_gpgpu == v);
}