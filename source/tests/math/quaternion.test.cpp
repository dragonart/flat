#include <catch.hpp>

#include "flat/math/quaternion.h"
#include "flat/math/vector.h"


using namespace flat;


TEST_CASE("Quaternion: default constructor", "[Quaternion]")
{
    Quaternion q;
    REQUIRE(q.x == 0.0f);
    REQUIRE(q.y == 0.0f);
    REQUIRE(q.z == 0.0f);
    REQUIRE(q.w == 0.0f);
}

TEST_CASE("Quaternion: constructor from 4 floats", "[Quaternion]")
{
    Quaternion q(1.0f, 2.0f, 3.0f, 4.0f);
    REQUIRE(q.x == 1.0f);
    REQUIRE(q.y == 2.0f);
    REQUIRE(q.z == 3.0f);
    REQUIRE(q.w == 4.0f);
}

TEST_CASE("Quaternion: constructor from Quaternion", "[Quaternion]")
{
    Quaternion w(1.0f, 2.0f, 3.0f, 4.0f);

    Quaternion v(w);
    REQUIRE(v.x == 1.0f);
    REQUIRE(v.y == 2.0f);
    REQUIRE(v.z == 3.0f);
    REQUIRE(v.w == 4.0f);
}

TEST_CASE("Quaternion: operator = (Quaternion)", "[Quaternion]")
{
    Quaternion v(1.0f, 2.0f, 3.0f, 4.0f);

    auto w = v;
    REQUIRE(w.x == 1.0f);
    REQUIRE(w.y == 2.0f);
    REQUIRE(w.z == 3.0f);
    REQUIRE(w.w == 4.0f);
}


TEST_CASE("Quaternion: operator == (Quaternion)", "[Quaternion]")
{
    Quaternion v(1.0f, 2.0f, 3.0f, 4.0f);
    Quaternion w(1.0f, 2.0f, 3.0f, 4.0f);

    REQUIRE(v == w);
}

TEST_CASE("Quaternion: operator != (Quaternion)", "[Quaternion]")
{
    Quaternion v(1.0f, 2.0f, 3.0f, 4.0f);
    Quaternion w(1.5f, 2.0f, 3.0f, 4.0f);

    REQUIRE(v != w);
}


TEST_CASE("Quaternion: operator + (Quaternion)", "[Quaternion]")
{
    Quaternion v1(1.0f, 2.0f, 3.0f, 4.0f);
    Quaternion v2(4.5f, 5.5f, 6.5f, 2.0f);

    auto w = v1 + v2;
    REQUIRE(w.x == Approx(5.5f));
    REQUIRE(w.y == Approx(7.5f));
    REQUIRE(w.z == Approx(9.5f));
    REQUIRE(w.w == Approx(6.0f));
}

TEST_CASE("Quaternion: operator += (Quaternion)", "[Quaternion]")
{
    Quaternion v(1.0f, 2.0f, 3.0f, 4.0f);
    Quaternion w(4.5f, 5.5f, 6.5f, 2.0f);

    v += w;
    REQUIRE(v.x == Approx(5.5f));
    REQUIRE(v.y == Approx(7.5f));
    REQUIRE(v.z == Approx(9.5f));
    REQUIRE(v.w == Approx(6.0f));
}

TEST_CASE("Quaternion: operator - (Quaternion)", "[Quaternion]")
{
    Quaternion v1(1.0f, 2.0f, 3.0f, 2.0f);
    Quaternion v2(4.5f, 5.3f, 6.2f, 4.0f);

    auto w = v2 - v1;
    REQUIRE(w.x == Approx(3.5f));
    REQUIRE(w.y == Approx(3.3f));
    REQUIRE(w.z == Approx(3.2f));
    REQUIRE(w.w == Approx(2.0f));
}

TEST_CASE("Quaternion: operator -= (Quaternion)", "[Quaternion]")
{
    Quaternion v(1.0f, 2.0f, 3.0f, 2.0f);
    Quaternion w(4.5f, 5.5f, 6.5f, 4.0f);

    v -= w;
    REQUIRE(v.x == Approx(-3.5f));
    REQUIRE(v.y == Approx(-3.5f));
    REQUIRE(v.z == Approx(-3.5f));
    REQUIRE(v.w == Approx(-2.0f));
}

TEST_CASE("Quaternion: operator * (float)", "[Quaternion]")
{
    Quaternion v(1.0f, 2.0f, 3.0f, 4.0f);

    Quaternion w = v * 2.5f;
    REQUIRE(w.x == Approx(2.5f));
    REQUIRE(w.y == Approx(5.0f));
    REQUIRE(w.z == Approx(7.5f));
    REQUIRE(w.w == Approx(10.0f));
}

TEST_CASE("Quaternion: operator *= (float)", "[Quaternion]")
{
    Quaternion v(1.0f, 2.0f, 3.0f, 4.0f);

    v *= 2.5f;
    REQUIRE(v.x == Approx(2.5f));
    REQUIRE(v.y == Approx(5.0f));
    REQUIRE(v.z == Approx(7.5f));
    REQUIRE(v.w == Approx(10.0f));
}

TEST_CASE("Quaternion: operator / (float)", "[Quaternion]")
{
    Quaternion v(2.5f, 5.0f, 7.5f, 10.0f);

    auto w = v / 2.5f;
    REQUIRE(w.x == Approx(1.0f));
    REQUIRE(w.y == Approx(2.0f));
    REQUIRE(w.z == Approx(3.0f));
    REQUIRE(w.w == Approx(4.0f));
}

TEST_CASE("Quaternion: operator /= (float)", "[Quaternion]")
{
    Quaternion v(2.5f, 5.0f, 7.5f, 10.0f);

    v /= 2.5f;
    REQUIRE(v.x == Approx(1.0f));
    REQUIRE(v.y == Approx(2.0f));
    REQUIRE(v.z == Approx(3.0f));
    REQUIRE(v.w == Approx(4.0f));
}


TEST_CASE("Quaternion: normalize()", "[Quaternion]")
{
    Quaternion v(3.1f, 4.87f, 9.23f, -11.2f);
    v = Quaternion::normalize(v);

    REQUIRE(Quaternion::length(v) == Approx(1.0f));
}

TEST_CASE("Quaternion: conjugate()", "[Quaternion]")
{
    Quaternion v(1.5f, 2.5f, 5.5f, 6.1f);

    auto w = Quaternion::conjugate(v);
    REQUIRE(w.x == Approx(-1.5f));
    REQUIRE(w.y == Approx(-2.5f));
    REQUIRE(w.z == Approx(-5.5f));
    REQUIRE(w.w == Approx(6.1f));
}

TEST_CASE("Quaternion: inverse()", "[Quaternion]")
{
    Quaternion v(1.0f, 2.0f, 3.0f, 4.0f);

    auto w = Quaternion::inverse(v);
    REQUIRE(w.x == Approx(-0.033333f));
    REQUIRE(w.y == Approx(-0.066667f));
    REQUIRE(w.z == Approx(-0.099999f));
    REQUIRE(w.w == Approx(0.1333333f));
}


TEST_CASE("Quaternion: norm()", "[Quaternion]")
{
    Quaternion v(1.0f, 2.0f, 3.0f, 4.0f);

    REQUIRE(Quaternion::norm(v) == Approx(30.0f));
}

TEST_CASE("Quaternion: length()", "[Quaternion]")
{
    Quaternion v(1.0f, 2.0f, 3.0f, 4.0f);

    REQUIRE(Quaternion::length(v) == Approx(5.4772f));
}


TEST_CASE("Quaternion: dot()", "[Quaternion]")
{
    Quaternion v(1.5f, 2.5f, 5.5f, 2.0f);
    Quaternion w(4.0f, 2.0f, 9.0f, 5.0f);

    REQUIRE(Quaternion::dot(v, w) == Approx(70.5f));
}

TEST_CASE("Quaternion: cross(quaternion, quaternion)", "[Quaternion]")
{
    Quaternion x(1.0f, 2.0f, 3.0f, 4.0f);
    Quaternion y(5.0f, 6.0f, 7.0f, 8.0f);

    auto z = Quaternion::cross(x, y);
    REQUIRE(z.x == Approx(24.0f));
    REQUIRE(z.y == Approx(48.0f));
    REQUIRE(z.z == Approx(48.0f));
    REQUIRE(z.w == Approx(-6.0f));
}

TEST_CASE("Quaternion: cross(quaternion, vector)", "[Quaternion")
{
    Quaternion q(1.0f, 2.0f, 3.0f, 4.0f);
    Vector v(5.0f, 6.0f, 7.0f);

    auto z = Quaternion::cross(q, v);
    REQUIRE(z.x == Approx(16.0f));
    REQUIRE(z.y == Approx(32.0f));
    REQUIRE(z.z == Approx(24.0f));
    REQUIRE(z.w == Approx(-38.0f));
}

TEST_CASE("Quaternion: size()", "[Quaternion]")
{
    Quaternion v;

    REQUIRE(v.size(Serializable::Type::MinimalSize) == sizeof(float) * 4);
    REQUIRE(v.size(Serializable::Type::WithAlignment) == sizeof(float) * 4);

    REQUIRE_THROWS(v.size(Serializable::Type::Unknown));
}

TEST_CASE("Quaternion: serialize() / deserialize() - with vector", "[Quaternion]")
{
    Quaternion v(1.1f, 2.2f, 3.3f, 6.6f);

    auto data_for_network = v.serialize(Serializable::Type::MinimalSize);
    REQUIRE(data_for_network.size() == sizeof(float) * 4);

    Quaternion deserialized_from_network;
    deserialized_from_network.deserialize(data_for_network, Serializable::Type::MinimalSize);
    REQUIRE(deserialized_from_network == v);

    auto data_for_gppgu = v.serialize(Serializable::Type::WithAlignment);
    REQUIRE(data_for_gppgu.size() == sizeof(float) * 4);

    Quaternion deserialized_from_gpgpu;
    deserialized_from_gpgpu.deserialize(data_for_gppgu, Serializable::Type::WithAlignment);
    REQUIRE(deserialized_from_gpgpu == v);
}

TEST_CASE("Quaternion: serialize() / deserialize() - with pointer", "[Quaternion]")
{
    Quaternion v(1.1f, 2.2f, 3.3f, 6.6f);

    vector<unsigned char> data_for_network(v.size(Serializable::Type::MinimalSize));
    v.serialize(data_for_network.data(), Serializable::Type::MinimalSize);

    Quaternion deserialized_from_network;
    deserialized_from_network.deserialize(data_for_network.data(), Serializable::Type::MinimalSize);
    REQUIRE(deserialized_from_network == v);

    vector<unsigned char> data_for_gpgpu(v.size(Serializable::Type::WithAlignment));
    v.serialize(data_for_gpgpu.data(), Serializable::Type::WithAlignment);

    Quaternion deserialized_from_gpgpu;
    deserialized_from_gpgpu.deserialize(data_for_gpgpu.data(), Serializable::Type::WithAlignment);
    REQUIRE(deserialized_from_gpgpu == v);
}