#include <catch.hpp>

#include "flat/graphics/color.h"


using namespace flat;


TEST_CASE("Color: size()", "[Color]")
{
    Color color;

    REQUIRE(color.size(Serializable::Type::MinimalSize) == 4);
    REQUIRE(color.size(Serializable::Type::WithAlignment) == 4);

    REQUIRE_THROWS(color.size(Serializable::Type::Unknown));
}

TEST_CASE("Color: serialize() / deserialize() - with vector", "[Color]")
{
    Color color;

    auto data_for_network = color.serialize(Serializable::Type::MinimalSize);
    REQUIRE(data_for_network.size() == color.size(Serializable::Type::MinimalSize));

    Color deserialized_from_network;
    deserialized_from_network.deserialize(data_for_network, Serializable::Type::MinimalSize);
    REQUIRE(deserialized_from_network == color);

    auto data_for_gpgpu = color.serialize(Serializable::Type::WithAlignment);
    REQUIRE(data_for_gpgpu.size() == color.size(Serializable::Type::WithAlignment));

    Color deserialized_from_gpgpu;
    deserialized_from_gpgpu.deserialize(data_for_gpgpu, Serializable::Type::WithAlignment);
    REQUIRE(deserialized_from_gpgpu == color);
}

TEST_CASE("Color: serialize() / deserialize() - with pointer", "[Color]")
{
    Color color;

    vector<unsigned char> data_for_network(color.size(Serializable::Type::MinimalSize));
    color.serialize(data_for_network.data(), Serializable::Type::MinimalSize);

    Color deserialized_from_network;
    deserialized_from_network.deserialize(data_for_network.data(), Serializable::Type::MinimalSize);
    REQUIRE(deserialized_from_network == color);

    vector<unsigned char> data_for_gpgpu(color.size(Serializable::Type::WithAlignment));
    color.serialize(data_for_gpgpu.data(), Serializable::Type::WithAlignment);

    Color deserialized_from_gpgpu;
    deserialized_from_gpgpu.deserialize(data_for_gpgpu.data(), Serializable::Type::WithAlignment);
    REQUIRE(deserialized_from_gpgpu == color);
}