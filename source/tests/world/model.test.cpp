#include <catch.hpp>


#include "flat/world/model.h"


using namespace flat;


TEST_CASE("Model: default constructor", "[Model]")
{
    Model model;

    REQUIRE(model.total_nodes_number() == 0);
}

TEST_CASE("Model: load(chess-cube.octree)", "[Model]")
{
    Model model;
    model.load("../data/models/chess-cube.octree");

    REQUIRE(model.total_nodes_number() == 9);

    auto data = model.data();

    REQUIRE(data[0].color == Color(0xff, 0x00, 0x00));
    REQUIRE_FALSE(data[0].monolith);

    for(unsigned char i = 0; i < 8; ++i)
        REQUIRE(data[0].leaves[i] == i+1);

    REQUIRE(data[data[0].leaves[0]].color == Color(0x00, 0x00, 0xff));
    REQUIRE(data[data[0].leaves[1]].color == Color(0xff, 0xff, 0x00));
    REQUIRE(data[data[0].leaves[2]].color == Color(0xff, 0xff, 0x00));
    REQUIRE(data[data[0].leaves[3]].color == Color(0x00, 0x00, 0xff));
    REQUIRE(data[data[0].leaves[4]].color == Color(0xff, 0xff, 0x00));
    REQUIRE(data[data[0].leaves[5]].color == Color(0x00, 0x00, 0xff));
    REQUIRE(data[data[0].leaves[6]].color == Color(0x00, 0x00, 0xff));
    REQUIRE(data[data[0].leaves[7]].color == Color(0xff, 0xff, 0x00));
    for(unsigned char i = 0; i < 8; ++i)
    {
        REQUIRE(data[data[0].leaves[i]].monolith);
        for (unsigned char j = 0; j < 8; ++j)
            REQUIRE(data[data[0].leaves[i]].leaves[j] == 0);
    }

    model.release();
}

TEST_CASE("Model: size()", "[Model]")
{
    Model model;
    model.load("../data/models/chess-cube.octree");

    REQUIRE(model.size(Serializable::Type::MinimalSize) == sizeof(size_t) + 69*9);
    REQUIRE(model.size(Serializable::Type::WithAlignment) == sizeof(size_t) + 72*9);

    REQUIRE_THROWS(model.size(Serializable::Type::Unknown));
}

TEST_CASE("Model: serialize() / deserialize() - with vector", "[Model]")
{
    Model model;
    model.load("../data/models/chess-cube.octree");

    auto data_for_network = model.serialize(Serializable::Type::MinimalSize);
    REQUIRE(data_for_network.size() == model.size(Serializable::Type::MinimalSize));

    auto size_pointer = reinterpret_cast<size_t*>(data_for_network.data());
    REQUIRE(*size_pointer == 9);

    Model deserialized_from_network;
    deserialized_from_network.deserialize(data_for_network, Serializable::Type::MinimalSize);
    REQUIRE(deserialized_from_network.data().size() == 9);
    REQUIRE(deserialized_from_network == model);

    auto data_for_gpgpu = model.serialize(Serializable::Type::WithAlignment);
    REQUIRE(data_for_gpgpu.size() == model.size(Serializable::Type::WithAlignment));

    Model deserialized_from_gpgpu;
    deserialized_from_gpgpu.deserialize(data_for_gpgpu, Serializable::Type::WithAlignment);
    REQUIRE(deserialized_from_gpgpu == model);
}

TEST_CASE("Model: serialize() / deserialize() - with pointer", "[Model]")
{
    Model model;
    model.load("../data/models/chess-cube.octree");

    vector<unsigned char> data_for_network(model.size(Serializable::Type::MinimalSize));
    model.serialize(data_for_network.data(), Serializable::Type::MinimalSize);

    Model deserialized_from_network;
    deserialized_from_network.deserialize(data_for_network.data(), Serializable::Type::MinimalSize);
    REQUIRE(deserialized_from_network == model);

    vector<unsigned char> data_for_gpgpu(model.size(Serializable::Type::WithAlignment));
    model.serialize(data_for_gpgpu.data(), Serializable::Type::WithAlignment);

    Model deserialized_from_gpgpu;
    deserialized_from_gpgpu.deserialize(data_for_gpgpu.data(), Serializable::Type::WithAlignment);
    REQUIRE(deserialized_from_gpgpu == model);
}