#include <catch.hpp>

#include "flat/world/octree.h"


using namespace flat;


TEST_CASE("Octree: size()", "[Octree]")
{
    Octree octree;

    REQUIRE(octree.size(Serializable::Type::MinimalSize) == 69);
    REQUIRE(octree.size(Serializable::Type::WithAlignment) == 72);

    REQUIRE_THROWS(octree.size(Serializable::Type::Unknown));
}

TEST_CASE("Octree: serialize() / deserialize() - with vector", "[Octree]")
{
    Octree octree;

    auto data_for_network = octree.serialize(Serializable::Type::MinimalSize);
    REQUIRE(data_for_network.size() == octree.size(Serializable::Type::MinimalSize));

    Octree deserialized_from_network;
    deserialized_from_network.deserialize(data_for_network, Serializable::Type::MinimalSize);
    REQUIRE(deserialized_from_network == octree);

    auto data_for_gpgpu = octree.serialize(Serializable::Type::WithAlignment);
    REQUIRE(data_for_gpgpu.size() == octree.size(Serializable::Type::WithAlignment));

    Octree deserialized_from_gpgpu;
    deserialized_from_gpgpu.deserialize(data_for_gpgpu, Serializable::Type::WithAlignment);
    REQUIRE(deserialized_from_gpgpu == octree);
}

TEST_CASE("Octree: serialize() / deserialize() - with pointer", "[Octree]")
{
    Octree octree;

    vector<unsigned char> data_for_network(octree.size(Serializable::Type::MinimalSize));
    octree.serialize(data_for_network.data(), Serializable::Type::MinimalSize);

    Octree deserialized_from_network;
    deserialized_from_network.deserialize(data_for_network.data(), Serializable::Type::MinimalSize);
    REQUIRE(deserialized_from_network == octree);

    vector<unsigned char> data_for_gpgpu(octree.size(Serializable::Type::WithAlignment));
    octree.serialize(data_for_gpgpu.data(), Serializable::Type::WithAlignment);

    Octree deserialized_from_gpgpu;
    deserialized_from_gpgpu.deserialize(data_for_gpgpu.data(), Serializable::Type::WithAlignment);
    REQUIRE(deserialized_from_gpgpu == octree);
}