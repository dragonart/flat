#include <catch.hpp>

#include "flat/world/object.h"


using namespace flat;


TEST_CASE("Object: size()", "[Object]")
{
    Object object;

    REQUIRE(object.size(Serializable::Type::MinimalSize) == sizeof(unsigned long long) + sizeof(float) * 8);
    REQUIRE(object.size(Serializable::Type::WithAlignment) == sizeof(unsigned long long) + sizeof(float) * 12);

    REQUIRE_THROWS(object.size(Serializable::Type::Unknown));
}

TEST_CASE("Object: serialize() / deserialize() - with vector", "[Object]")
{
    Object object;
    object.model_id() = 4532;
    object.obb().center.z = 2.3f;
    object.obb().half_size = -1.0f;
    object.obb().quaternion.y = -28.1f;

    auto data_for_network = object.serialize(Serializable::Type::MinimalSize);
    REQUIRE(data_for_network.size() == object.size(Serializable::Type::MinimalSize));

    Object deserialized_from_network;
    deserialized_from_network.deserialize(data_for_network, Serializable::Type::MinimalSize);
    REQUIRE(deserialized_from_network == object);

    auto data_for_gpgpu = object.serialize(Serializable::Type::WithAlignment);
    REQUIRE(data_for_gpgpu.size() == object.size(Serializable::Type::WithAlignment));

    Object deserialized_from_gpgpu;
    deserialized_from_gpgpu.deserialize(data_for_gpgpu, Serializable::Type::WithAlignment);
    REQUIRE(deserialized_from_gpgpu == object);
}

TEST_CASE("Object: serialize() / deserialize() - with pointer", "[Object]")
{
    Object object;

    vector<unsigned char> data_for_network(object.size(Serializable::Type::MinimalSize));
    object.serialize(data_for_network.data(), Serializable::Type::MinimalSize);

    Object deserialized_from_network;
    deserialized_from_network.deserialize(data_for_network.data(), Serializable::Type::MinimalSize);
    REQUIRE(deserialized_from_network == object);

    vector<unsigned char> data_for_gpgpu(object.size(Serializable::Type::WithAlignment));
    object.serialize(data_for_gpgpu.data(), Serializable::Type::WithAlignment);

    Object deserialized_from_gpgpu;
    deserialized_from_gpgpu.deserialize(data_for_gpgpu.data(), Serializable::Type::WithAlignment);
    REQUIRE(deserialized_from_gpgpu == object);
}