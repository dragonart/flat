#include <catch.hpp>

#include "flat/world/camera.h"


using namespace flat;


TEST_CASE("Camera: size()", "[Camera]")
{
    Camera camera;

    REQUIRE(camera.size(Serializable::Type::MinimalSize) == sizeof(float) * 13);
    REQUIRE(camera.size(Serializable::Type::WithAlignment) == sizeof(float) * 20);

    REQUIRE_THROWS(camera.size(Serializable::Type::Unknown));
}

TEST_CASE("Camera: serialize() / deserialize() - with vector", "[Camera]")
{
    Camera camera;
    camera.setFov(3.2f);

    auto data_for_network = camera.serialize(Serializable::Type::MinimalSize);
    REQUIRE(data_for_network.size() == camera.size(Serializable::Type::MinimalSize));

    Camera deserialized_from_network;
    deserialized_from_network.deserialize(data_for_network, Serializable::Type::MinimalSize);
    REQUIRE(deserialized_from_network == camera);

    auto data_for_gpgpu = camera.serialize(Serializable::Type::WithAlignment);
    REQUIRE(data_for_gpgpu.size() == camera.size(Serializable::Type::WithAlignment));

    Camera deserialized_from_gpgpu;
    deserialized_from_gpgpu.deserialize(data_for_gpgpu, Serializable::Type::WithAlignment);
    REQUIRE(deserialized_from_gpgpu == camera);
}

TEST_CASE("Camera: serialize() / deserialize() - with pointer", "[Camera]")
{
    Camera camera;

    vector<unsigned char> data_for_network(camera.size(Serializable::Type::MinimalSize));
    camera.serialize(data_for_network.data(), Serializable::Type::MinimalSize);

    Camera deserialized_from_network;
    deserialized_from_network.deserialize(data_for_network.data(), Serializable::Type::MinimalSize);
    REQUIRE(deserialized_from_network == camera);

    vector<unsigned char> data_for_gpgpu(camera.size(Serializable::Type::WithAlignment));
    camera.serialize(data_for_gpgpu.data(), Serializable::Type::WithAlignment);

    Camera deserialized_from_gpgpu;
    deserialized_from_gpgpu.deserialize(data_for_gpgpu.data(), Serializable::Type::WithAlignment);
    REQUIRE(deserialized_from_gpgpu == camera);
}