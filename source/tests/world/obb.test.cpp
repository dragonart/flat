#include <catch.hpp>

#include "flat/world/obb.h"


using namespace flat;


TEST_CASE("Obb: size()", "[Obb]")
{
    Obb obb;

    REQUIRE(obb.size(Serializable::Type::MinimalSize) == sizeof(float) * 8);
    REQUIRE(obb.size(Serializable::Type::WithAlignment) == sizeof(float) * 12);

    REQUIRE_THROWS(obb.size(Serializable::Type::Unknown));
}

TEST_CASE("Obb: serialize() / deserialize() - with vector", "[Obb]")
{
    Obb obb;
    obb.center.x = 83.0f;
    obb.half_size = 11.3f;
    obb.quaternion.y = 16.9f;

    auto data_for_network = obb.serialize(Serializable::Type::MinimalSize);
    REQUIRE(data_for_network.size() == obb.size(Serializable::Type::MinimalSize));

    Obb deserialized_from_network;
    deserialized_from_network.deserialize(data_for_network, Serializable::Type::MinimalSize);
    REQUIRE(deserialized_from_network == obb);

    auto data_for_gpgpu = obb.serialize(Serializable::Type::WithAlignment);
    REQUIRE(data_for_gpgpu.size() == obb.size(Serializable::Type::WithAlignment));

    Obb deserialized_from_gpgpu;
    deserialized_from_gpgpu.deserialize(data_for_gpgpu, Serializable::Type::WithAlignment);
    REQUIRE(deserialized_from_gpgpu == obb);
}

TEST_CASE("Obb: serialize() / deserialize() - with pointer", "[Obb]")
{
    Obb obb;

    vector<unsigned char> data_for_network(obb.size(Serializable::Type::MinimalSize));
    obb.serialize(data_for_network.data(), Serializable::Type::MinimalSize);

    Obb deserialized_from_network;
    deserialized_from_network.deserialize(data_for_network.data(), Serializable::Type::MinimalSize);
    REQUIRE(deserialized_from_network == obb);

    vector<unsigned char> data_for_gpgpu(obb.size(Serializable::Type::WithAlignment));
    obb.serialize(data_for_gpgpu.data(), Serializable::Type::WithAlignment);

    Obb deserialized_from_gpgpu;
    deserialized_from_gpgpu.deserialize(data_for_gpgpu.data(), Serializable::Type::WithAlignment);
    REQUIRE(deserialized_from_gpgpu == obb);
}