#pragma once


#include <string>


using std::string;
using std::to_string;


namespace flat
{
    struct Version
    {
        enum class EnvironmentType: unsigned char
        {
            Stable,
            Alpha,
            Beta,
            ReleaseCandidate,
            HotFix
        };


        static const unsigned int Major = 3;
        static const unsigned int Minor = 1;
        static const unsigned int Patch = 0;
        static const unsigned int Feature = 5;
        static const unsigned long Build = 225;
        static const EnvironmentType Environment = EnvironmentType::ReleaseCandidate;


        static const string getLine()
        {
            string feature;
            string environment;
            switch(Environment)
            {
                case EnvironmentType::Stable:
                    feature = "";
                    environment = "stable";
                    break;

                case EnvironmentType::Alpha:
                    feature = "." + to_string(Feature);
                    environment = "alpha";
                    break;

                case EnvironmentType::Beta:
                    feature = "." + to_string(Feature);
                    environment = "beta";
                    break;

                case EnvironmentType::ReleaseCandidate:
                    feature = "";
                    environment = "release candidate";
                    break;

                case EnvironmentType::HotFix:
                    feature = "";
                    environment = "hot fix";
                    break;
            }

            string total = to_string(Major) + "." + to_string(Minor) + "." + to_string(Patch) + feature + " #" + to_string(Build) + " " + environment;
            return total;
        }
    };
}
