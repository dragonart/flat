#include "core.h"


using namespace flat;


Core::~Core()
{ }

int Core::main(int argc,
               char** argv)
{
    try
    {
        initialize(argc, argv);

        while (_not_stop)
            update();
    }
    catch (exception& e)
    {
        processException(e);
        release();

        return -1;
    }

    release();

    return 0;
}