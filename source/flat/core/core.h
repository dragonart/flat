#pragma once


#include <stdexcept>


using std::exception;


namespace flat
{
    class Core
    {
    public:
        virtual ~Core();

        int main(int argc,
                 char** argv);


    protected:
        bool _not_stop;


    protected:
        virtual void initialize(int argc,
                                char** argv) = 0;
        virtual void release() = 0;

        virtual void update() = 0;
        virtual void processException(exception& e) = 0;
    };
}