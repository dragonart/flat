#pragma once


#include <memory>

#include <CL/cl.h>

#include "graphics/frame.h"
#include "camera.h"
#include "object.h"


namespace flat
{
    class Program;
    class Buffer;


    class World : public Executor
    {
    public:
        int execute(Command command) override;

        void initialize();
        void release();

        void update();
        double prepareFrame(Frame& frame);


    private:
        shared_ptr<Program> _program;
        shared_ptr<Kernel> _calculate_frame_kernel;

        vector<Object> _objects;
        shared_ptr<Buffer> _objects_buffer;

        Model _model;
        shared_ptr<Buffer> _model_buffer;

        Camera _camera;
        shared_ptr<Buffer> _camera_buffer;
        Size _last_frame_size;
        shared_ptr<Buffer> _last_frame_buffer;


    private:
        void updateCameraOnGpgpu();
    };
}