#pragma once


#include "command/executor.h"
#include "math/vector.h"
#include "utility/serializable.h"


namespace flat
{
    class Camera : public Executor,
                   public Serializable
    {
        static constexpr float Step = 0.05f;
        static constexpr float RotationDelta = 0.01f;


    public:
        Camera(Vector position = Vector(0.0f, 0.0f, 0.0f),
               Vector direction = Vector(0.0f, 1.0f, 0.0f),
               Vector up = Vector(0.0f, 0.0f, 1.0f),
               Vector right = Vector(1.0f, 0.0f, 0.0f),
               float fov = 3.14159265f / 4.0f);

        int execute(Command command) override;

        bool operator==(const Camera& other) const;
        bool operator!=(const Camera& other) const;

        void setPosition(Vector position); // TODO: clear such ugly interface
        void setDirection(Vector direction);
        void setUp(Vector up);
        void setRight(Vector right);
        void setFov(float fov);

        const Vector& getPosition() const;
        const Vector& getDirection() const;
        const Vector& getUp() const;
        const Vector& getRight() const;
        float getFov() const;

        vector<unsigned char> serialize(Type type) const override; // TODO: add 'virtual' to make Camera able to be a parent :)
        void serialize(unsigned char* pointer,
                       Type type) const override;
        void deserialize(const vector<unsigned char>& data,
                         Type type) override;
        void deserialize(const unsigned char* pointer,
                         Type type) override;
        size_t size(Type type) const override;


    private:
        Vector _position;
        Vector _direction;
        Vector _up;
        Vector _right;

        float _fov;
    };
}