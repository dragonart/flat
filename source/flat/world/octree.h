#pragma once


#include "graphics/color.h"
#include "utility/serializable.h"


namespace flat
{
    class Octree : public Serializable
    {
    public:
        Color color; // TODO: protect fields
        bool monolith;
        unsigned long long leaves[8];


    public:
        Octree(Color color = Color());
        virtual ~Octree();

        bool operator==(const Octree& other) const;
        bool operator!=(const Octree& other) const;


        virtual vector<unsigned char> serialize(Type type) const override;
        virtual void serialize(unsigned char* pointer, Type type) const override;
        virtual void deserialize(const vector<unsigned char>& data, Type type) override;
        virtual void deserialize(const unsigned char* pointer, Type type) override;
        virtual size_t size(Type type) const override;
    };
}