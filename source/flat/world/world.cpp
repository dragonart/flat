#include <chrono>
#include <iostream>


#include "gpgpu/gpgpu-provider.h"
#include "world.h"


using namespace flat;

using std::cout;
using std::endl;
using std::string;
using std::to_string;
using std::runtime_error;
using std::logic_error;
using std::make_shared;


int World::execute(Command command)
{
    if (command.header() == "camera")
    {
        _camera.execute(command.withoutHeader());
        updateCameraOnGpgpu();
        return 0;
    }

    throw logic_error("Unknown command for World: " + command.totalString());
}


void World::initialize()
{
    string source =
            "struct Color\n" \
            "{\n" \
            "   uchar red, green, blue, alpha;\n" \
            "};\n" \
            "\n" \
            "struct Obb\n" \
            "{\n" \
            "   float3 center;\n" \
            "   float half_size;\n" \
            "   float4 quaternion;\n" \
            "};\n" \
            "\n" \
            "struct Octree\n" \
            "{\n" \
            "   struct Color color;\n" \
            "   uchar monolith;\n" \
            "   ulong leaves[8];\n" \
            "};\n" \
            "\n" \
            "struct Ray\n" \
            "{\n" \
            "   float3 origin;\n" \
            "   float3 direction;\n" \
            "};\n" \
            "\n" \
            "struct Camera\n" \
            "{\n" \
            "   float3 position;\n" \
            "   float3 direction;\n" \
            "   float3 up;\n" \
            "   float3 right;\n" \
            "   float fov;\n" \
            "};\n" \
            "\n" \
            "struct __attribute__((packed)) Object\n" \
            "{\n" \
            "   ulong model_start;\n" \
            "   struct Obb obb;\n" \
            "};\n" \
            "\n" \
            "struct TracingResult\n" \
            "{\n" \
            "   float distance;\n" \
            "   __global struct Octree* node_ptr;\n" \
            "   uint object_index;\n" \
            "};\n" \
            "\n" \
            "struct TracingPack\n" \
            "{\n" \
            "   struct Ray ray;\n" \
            "   __global struct Object* object_ptr;\n" \
            "   __global struct Octree* node_ptr;\n" \
            "   uint object_index;\n" \
            "   bool intersects;\n" \
            "   float t_min;\n" \
            "   float t_max;\n" \
            "};\n" \
            "\n" \
            "float4 constructQuaternion(float4 axis_with_angle);\n" \
            "float4 inverseQuaternion(float4 q);\n" \
            "float4 quaternionCrossQQ(float4 one, float4 another);\n" \
            "float4 quaternionCrossQV(float4 one, float3 another);\n" \
            "float3 rotateWithQuaternion(float3 v, float4 q, bool inverse);\n" \
            "struct Ray calculateRay(uint x, uint y, uint frame_width, uint frame_height, __global struct Camera* camera_ptr);\n" \
            "bool intersects(struct Ray* ray_ptr, float half_size, float* t_min_ptr, float* t_max_ptr);\n" \
            "uchar findContainingLeaf(float3* center_ptr, float* half_size_ptr, float3* target_ptr);\n" \
            "bool checkThatObbContains(float half_size, float3* target_ptr);\n" \
            "void initializeTracingPacks(struct TracingPack* packs, struct Ray* ray_ptr, __global struct Object* objects, uint objects_number);\n" \
            "void sortTracingPacks(struct TracingPack* packs, uint objects_number);\n" \
            "void clerifyTracingPack(struct TracingPack* pack_ptr, __global uchar* memory);\n" \
            "struct TracingResult traceRay(struct Ray* ray_ptr, __global struct Object* objects, uint objects_number, __global uchar* memory);\n" \
            "\n" \
            "float4 constructQuaternion(float4 axis_with_angle)\n" \
            "{\n" \
            "    float4 result;\n" \
            "    result.xyz = axis_with_angle.xyz * sin(axis_with_angle.w / 2.0f);\n" \
            "    result.w = cos(axis_with_angle.w / 2.0f);\n" \
            "    return result;\n" \
            "}\n" \
            "\n" \
            "float4 inverseQuaternion(float4 q)\n" \
            "{\n" \
            "    float4 result;\n" \
            "    float len = sqrt(q.x * q.x + q.y * q.y + q.z * q.z + q.w * q.w);\n" \
            "    result.xyz = -q.xyz / len;\n" \
            "    result.w = q.w / len;\n" \
            "   return result;\n" \
            "}\n" \
            "\n" \
            "float4 quaternionCrossQQ(float4 one,\n" \
            "                       float4 another)\n" \
            "{\n" \
            "    float4 result;\n" \
            "    result.xyz = cross(one.xyz, another.xyz).xyz + another.xyz * one.w + one.xyz * another.w;\n" \
            "    result.w = one.w * another.w - dot(one.xyz, another.xyz);\n" \
            "    return result;\n" \
            "}\n" \
            "\n" \
            "float4 quaternionCrossQV(float4 one,\n" \
            "                       float3 another)\n" \
            "{\n" \
            "    float4 result;\n" \
            "    result.xyz = cross(one.xyz, another).xyz + another.xyz * one.w;\n" \
            "    result.w = -dot(one.xyz, another);\n" \
            "    return result;\n" \
            "}\n" \
            "\n" \
            "float3 rotateWithQuaternion(float3 v,\n" \
            "                            float4 q,\n" \
            "                            bool inverse)\n" \
            "{\n" \
            "    float4 q_inversed = inverseQuaternion(q);\n" \
            "\n" \
            "    if (inverse)\n" \
            "        return quaternionCrossQQ(quaternionCrossQV(q_inversed, v), q).xyz;\n" \
            "\n" \
            "    return quaternionCrossQQ(quaternionCrossQV(q, v), q_inversed).xyz;\n" \
            "}\n" \
            "\n" \
            "struct Ray calculateRay(uint x,\n" \
            "                        uint y,\n" \
            "                        uint frame_width,\n" \
            "                        uint frame_height,\n" \
                                    "__global struct Camera* camera_ptr)\n" \
            "{\n" \
            "   const struct Camera camera = *camera_ptr;\n" \
            "   const float angle_up = (0.5f - (float) x / (float) frame_width) * camera.fov;\n" \
            "   const float angle_right = (0.5f - (float) y / (float) frame_height) * camera.fov * (float) frame_height / (float) frame_width;\n" \
            "\n" \
            "   float4 axis_with_angle;\n" \
            "   axis_with_angle.xyz = camera.right;\n" \
            "   axis_with_angle.w = angle_right;\n" \
            "   const float4 quaternion_right = constructQuaternion(axis_with_angle);\n" \
            "   axis_with_angle.xyz = camera.up;\n" \
            "   axis_with_angle.w = angle_up;\n" \
            "   const float4 quaternion_up = constructQuaternion(axis_with_angle);\n" \
            "   float3 ray_direction = rotateWithQuaternion(camera.direction, quaternion_right, false);\n" \
            "   ray_direction = rotateWithQuaternion(ray_direction, quaternion_up, false);\n" \
            "\n" \
            "   struct Ray ray;\n" \
            "   ray.origin = camera.position;\n" \
            "   ray.direction = ray_direction;\n" \
            "   return ray;\n" \
            "}\n" \
            "\n" \
            "bool intersects(struct Ray* ray_ptr,\n" \
            "                float half_size,\n" \
            "                float* t_min_ptr,\n" \
            "                float* t_max_ptr)\n" \
            "{\n" \
            "   struct Ray ray = *ray_ptr;\n" \
            "   float low, high;\n" \
            "   float t_min, t_max;\n" \
            "\n" \
            "   low  = (-half_size - ray.origin.x) / ray.direction.x;\n" \
            "   high = (+half_size - ray.origin.x) / ray.direction.x;\n" \
            "   t_min = min(low, high);\n" \
            "   t_max = max(low, high);\n" \
            "\n" \
            "   low  = (-half_size - ray.origin.y) / ray.direction.y;\n" \
            "   high = (+half_size - ray.origin.y) / ray.direction.y;\n" \
            "   t_min = max(t_min, min(low, high));\n" \
            "   t_max = min(t_max, max(low, high));\n" \
            "\n" \
            "   low  = (-half_size - ray.origin.z) / ray.direction.z;\n" \
            "   high = (+half_size - ray.origin.z) / ray.direction.z;\n" \
            "   t_min = max(t_min, min(low, high));\n" \
            "   t_max = min(t_max, max(low, high));\n" \
                "\n" \
            "   const bool intersects = (t_min < t_max) && (t_max > 0.0f);\n" \
            "\n" \
            "   if (intersects)\n" \
            "   {\n" \
            "       *t_min_ptr = t_min;\n" \
            "       *t_max_ptr = t_max;\n" \
            "       return true;\n" \
            "   }\n" \
            "\n" \
            "   *t_min_ptr = FLT_MAX;\n" \
            "   *t_max_ptr = FLT_MAX;\n" \
            "   return false;\n" \
            "}\n" \
            "\n" \
            "uchar findContainingLeaf(float3* center_ptr,\n" \
            "                         float* half_size_ptr,\n" \
            "                         float3* target_ptr)\n" \
            "{\n" \
            "   uchar index = 0;\n" \
            "\n" \
            "   float half_size = *half_size_ptr;\n" \
            "   float quater_size = half_size / 2.0f;\n" \
            "   float3 center; center.x = -quater_size; center.y = -quater_size; center.z = -quater_size;\n" \
            "\n" \
            "   if (target_ptr->x > 0.0f)\n" \
            "   {\n" \
            "       index = 1;\n" \
            "       center.x += half_size;\n" \
            "   }\n" \
            "   if (target_ptr->y > 0.0f)\n" \
            "   {\n" \
            "       index += 2;\n" \
            "       center.y += half_size;\n" \
            "   }\n" \
            "   if (target_ptr->z > 0.0f)\n" \
            "   {\n" \
            "       index += 4;\n" \
            "       center.z += half_size;\n" \
            "   }\n" \
            "\n" \
            "   *center_ptr = center;\n" \
            "   *half_size_ptr = quater_size;\n" \
            "   return index;\n" \
            "}\n" \
            "\n" \
            "void initializeTracingPacks(struct TracingPack* packs,\n" \
            "                            struct Ray* ray_ptr,\n" \
            "                            __global struct Object* objects,\n" \
            "                            uint objects_number)\n" \
            "{\n" \
            "   for (uint i = 0; i < objects_number; ++i)\n" \
            "   {\n" \
            "       packs[i].object_ptr = &objects[i];\n" \
            "       float3 center = objects[i].obb.center;\n" \
            "       packs[i].ray.origin = rotateWithQuaternion(ray_ptr->origin - center, objects[i].obb.quaternion, true);\n" \
            "       packs[i].ray.direction = rotateWithQuaternion(ray_ptr->direction, objects[i].obb.quaternion, true);\n" \
            "       packs[i].intersects = intersects(&packs[i].ray, objects[i].obb.half_size, &packs[i].t_min, &packs[i].t_max);\n" \
            "       packs[i].object_index = i;\n" \
            "   }\n" \
            "}\n" \
            "\n" \
            "void sortTracingPacks(struct TracingPack* packs,\n" \
            "                      uint objects_number)\n" \
            "{\n" \
            "   struct TracingPack temporary;\n" \
            "\n" \
            "   for (uint i = 1; i < objects_number; ++i)\n" \
            "       if (packs[i].t_min < packs[i - 1].t_min)\n" \
            "       {\n" \
            "           uint j = i - 1;\n" \
            "           while (packs[j].t_min > packs[j + 1].t_min)\n" \
            "           {\n" \
            "               temporary = packs[j];\n" \
            "               packs[j] = packs[j + 1];\n" \
            "               packs[j + 1] = temporary;\n" \
            "               if (j == 0)\n" \
            "                   break;\n" \
            "               j--;\n" \
            "           }\n" \
            "       }\n" \
            "}\n" \
            "\n" \
            "void clerifyTracingPack(struct TracingPack* pack_ptr,\n" \
            "                        __global uchar* memory)\n" \
            "{\n" \
            "   __global struct Octree* octree = &memory[pack_ptr->object_ptr->model_start + 8];\n" \
            "   if (octree[0].monolith)\n" \
            "   {\n" \
            "       pack_ptr->node_ptr = &octree[0];\n" \
            "       return;\n" \
            "   }\n" \
            "\n" \
            "   struct Ray ray = pack_ptr->ray;\n" \
            "   float t_min = pack_ptr->t_min;\n" \
            "   float t_max;\n" \
            "   if (t_min < 0.0f)\n" \
            "   {\n" \
            "       ray.origin -= ray.direction * t_min;\n" \
            "       t_min = 0.0f;\n" \
            "   }\n" \
            "\n" \
            "   float3 target = ray.origin + ray.direction * t_min;\n" \
            "   float3 center; center.x = 0.0f; center.y = 0.0f; center.z = 0.0f;\n" \
            "   float half_size = pack_ptr->object_ptr->obb.half_size;\n" \
            "   ulong node_index = 0;\n" \
            "\n" \
            "   struct TracingStackRecord\n" \
            "   {\n" \
            "       float3 center;\n" \
            "       float half_size;\n" \
            "       ulong node_index;\n" \
            "   } stack[255];\n" \
            "   uchar stack_size = 0;\n" \
            "\n" \
            "   while (true)\n" \
            "   {\n" \
            "       stack[stack_size].center = center;\n" \
            "       stack[stack_size].half_size = half_size;\n" \
            "       stack[stack_size++].node_index = node_index;\n" \
            "\n" \
            "       uchar leaf_index = findContainingLeaf(&center, &half_size, &target);\n" \
            "       ray.origin -= center;\n" \
            "       target -= center;\n" \
            "       node_index = octree[node_index].leaves[leaf_index];\n" \
            "\n" \
            "       if (node_index != 0)\n" \
            "       {\n" \
            "           if (octree[node_index].monolith)\n" \
            "           {\n" \
            "               pack_ptr->t_min = t_min;\n" \
            "               pack_ptr->t_max = t_max;\n" \
            "               pack_ptr->node_ptr = &octree[node_index];\n" \
            "               return;\n" \
            "           }\n" \
            "           continue;\n" \
            "       }\n" \
            "\n" \
            "       if (stack_size == 0)\n" \
            "       {\n" \
            "           pack_ptr->t_min = FLT_MAX;\n" \
            "           pack_ptr->t_max = FLT_MAX;\n" \
            "           pack_ptr->node_ptr = octree;\n" \
            "           pack_ptr->intersects = false;\n" \
            "           return;\n" \
            "       }\n" \
            "\n" \
            "       intersects(&ray, half_size, &t_min, &t_max);\n" \
            "       ray.origin += center;\n" \
            "       t_min = t_max + 0.001f;\n" \
            "       target = ray.origin + ray.direction * t_min;\n" \
            "\n" \
            "       node_index = stack[stack_size - 1].node_index;\n" \
            "       center = stack[stack_size - 1].center;\n" \
            "       half_size = stack[stack_size - 1].half_size;\n" \
            "       stack_size--;\n" \
            "\n" \
            "       while (!checkThatObbContains(half_size, &target))\n" \
            "       {\n" \
            "           if (stack_size == 0)\n" \
            "           {\n" \
            "               pack_ptr->t_min = FLT_MAX;\n" \
            "               pack_ptr->t_max = FLT_MAX;\n" \
            "               pack_ptr->node_ptr = octree;\n" \
            "               pack_ptr->intersects = false;\n" \
            "               return;\n" \
            "           }\n" \
            "\n" \
            "           node_index = stack[stack_size - 1].node_index;\n" \
            "           target += center;\n" \
            "           ray.origin += center;\n" \
            "           center = stack[stack_size - 1].center;\n" \
            "           half_size = stack[stack_size - 1].half_size;\n" \
            "           stack_size--;\n" \
            "       }\n" \
            "   }\n" \
            "}\n" \
            "\n" \
            "bool checkThatObbContains(float half_size,\n" \
            "                          float3* target_ptr)\n" \
            "{\n" \
            "   float3 target = *target_ptr;\n" \
            "   if(fabs(target.x) > half_size)\n" \
            "       return false;\n" \
            "   if(fabs(target.y) > half_size)\n" \
            "       return false;\n" \
            "   if(fabs(target.z) > half_size)\n" \
            "       return false;\n" \
            "   return true;\n" \
            "}\n" \
            "\n" \
            "struct TracingResult traceRay(struct Ray* ray_ptr,\n" \
            "                              __global struct Object* objects,\n" \
            "                              uint objects_number,\n" \
            "                              __global uchar* memory)\n" \
            "{\n" \
            "   struct TracingPack packs[255];\n" \
            "   initializeTracingPacks(packs, ray_ptr, objects, objects_number);\n" \
            "   sortTracingPacks(packs, objects_number);\n" \
            "\n" \
            "   float nearest_distance = FLT_MAX;\n" \
            "   uint nearest_index = 0;\n" \
            "\n" \
            "   for (uint i = 0; i < objects_number; ++i)\n" \
            "   {\n" \
            "       if (!packs[i].intersects)\n" \
            "           continue;\n" \
            "\n" \
            "       if (packs[i].t_min > nearest_distance)\n" \
            "           continue;\n" \
            "\n" \
            "       clerifyTracingPack(&packs[i], memory);\n" \
            "       if (!packs[i].intersects)\n" \
            "           continue;\n" \
            "       if (packs[i].t_min < nearest_distance)\n" \
            "       {\n" \
            "           nearest_distance = packs[i].t_min;\n" \
            "           nearest_index = i;\n" \
            "       }\n" \
            "   }\n" \
            "\n" \
            "   struct TracingResult result;\n" \
            "   result.distance = nearest_distance;\n" \
            "   result.node_ptr = packs[nearest_index].node_ptr;\n" \
            "   result.object_index = packs[nearest_index].object_index;\n" \
            "   return result;\n" \
            "}\n" \
            "\n" \
            "struct Buffer { ulong start; ulong size; };\n" \
            "__kernel void calculateFrame(struct Buffer frame_buffer,\n" \
            "                             uint frame_width,\n" \
            "                             uint frame_height,\n" \
            "                             struct Buffer camera_buffer,\n" \
            "                             struct Buffer objects_buffer,\n" \
            "                             uint objects_number,\n" \
            "                             __global uchar* memory)\n" \
            "{\n" \
            "   const uint x = get_global_id(0);\n" \
            "   const uint y = get_global_id(1);\n" \
            "   __global struct Camera* camera_ptr = &memory[camera_buffer.start];\n" \
            "   struct Ray ray = calculateRay(x, y, frame_width, frame_height, camera_ptr);\n" \
            "\n" \
            "   __global struct Object* objects_ptr = &memory[objects_buffer.start];\n" \
            "   const struct TracingResult result = traceRay(&ray, objects_ptr, objects_number, memory);\n" \
            "\n" \
            "   __global struct Color* frame = &memory[frame_buffer.start];\n" \
            "   if (result.distance == FLT_MAX)\n" \
            "   {\n" \
            "       frame[y * frame_width + x].red = 0x88;\n" \
            "       frame[y * frame_width + x].green = 0x88;\n" \
            "       frame[y * frame_width + x].blue = 0x88;\n" \
            "       frame[y * frame_width + x].alpha = 0xff;\n" \
            "   }\n" \
            "   else\n" \
            "       frame[y * frame_width + x] = result.node_ptr->color;\n" \
            "}";
    _program = GpgpuProvider::gpgpu()->createProgram(source);
    _calculate_frame_kernel = GpgpuProvider::gpgpu()->createKernel(_program, "calculateFrame");

    _last_frame_size = {10, 10};
    _last_frame_buffer = GpgpuProvider::gpgpu()->createBuffer(nullptr, sizeof(Color) * _last_frame_size.width * _last_frame_size.height);

    _camera.setPosition(Vector(0.0f, -10.0f, 0.0f));
    _camera_buffer = GpgpuProvider::gpgpu()->createBuffer(nullptr, _camera.size(Serializable::Type::WithAlignment));
    updateCameraOnGpgpu();

    _model.load("../data/models/mushrum.octree");
    _model_buffer = GpgpuProvider::gpgpu()->createBuffer(_model.serialize(Serializable::Type::WithAlignment).data(), _model.size(Serializable::Type::WithAlignment));

    Object object;
    object.obb().center = Vector(0.0f, 0.0f, 0.0f);
    object.obb().half_size = 0.5f;
    object.obb().quaternion = Quaternion(Vector::normalize(Vector(0.0f, 1.0f, 1.0f)), 0.0f);
    object.model_id() = _model_buffer->getStart();
    _objects.push_back(object);

    object.obb().center = Vector(2.0f, 0.0f, 0.0f);
    object.obb().quaternion = Quaternion(Vector::normalize(Vector(1.0f, 1.0f, 1.0f)), 0.3f);
    _objects.push_back(object);

    size_t total_objects_size = 0;
    for (unsigned int i = 0; i < _objects.size(); ++i)
        total_objects_size += _objects[i].size(Serializable::Type::WithAlignment);

    _objects_buffer = GpgpuProvider::gpgpu()->createBuffer(nullptr, total_objects_size);
    for (unsigned int i = 0; i < _objects.size(); ++i)
        GpgpuProvider::gpgpu()->writeBuffer(_objects_buffer, _objects[i].serialize(Serializable::Type::WithAlignment).data(), _objects[i].size(Serializable::Type::WithAlignment),
                                            _objects[0].size(Serializable::Type::WithAlignment) * i);

    cout << "World initialization succeeded." << endl;
}

void World::release()
{
    _model.release();
    cout << "World release succeeded." << endl;
}

void World::update()
{
    // world update
}

double World::prepareFrame(Frame& frame)
{
    auto start_time = std::chrono::system_clock::now();

    if (frame.getSize() != _last_frame_size)
    {
        _last_frame_size = frame.getSize();

        GpgpuProvider::gpgpu()->releaseBuffer(_last_frame_buffer);
        _last_frame_buffer = GpgpuProvider::gpgpu()->createBuffer(nullptr, sizeof(Color) * _last_frame_size.width * _last_frame_size.height);
    }

    GpgpuProvider::gpgpu()->setKernelArgument(_calculate_frame_kernel, 0, _last_frame_buffer);
    GpgpuProvider::gpgpu()->setKernelArgument(_calculate_frame_kernel, 1, &_last_frame_size.width, sizeof(unsigned int));
    GpgpuProvider::gpgpu()->setKernelArgument(_calculate_frame_kernel, 2, &_last_frame_size.height, sizeof(unsigned int));
    GpgpuProvider::gpgpu()->setKernelArgument(_calculate_frame_kernel, 3, _camera_buffer);
    GpgpuProvider::gpgpu()->setKernelArgument(_calculate_frame_kernel, 4, _objects_buffer);

    unsigned int objects_number = _objects.size();
    GpgpuProvider::gpgpu()->setKernelArgument(_calculate_frame_kernel, 5, &objects_number, sizeof(unsigned int));

    std::size_t size[3] = {_last_frame_size.width, _last_frame_size.height, 1};
    GpgpuProvider::gpgpu()->runKernel(_calculate_frame_kernel, 2, size);

    GpgpuProvider::gpgpu()->readBuffer(_last_frame_buffer, frame.getPixels().data());

    auto end_time = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed = end_time - start_time;

    return elapsed.count();
}


void World::updateCameraOnGpgpu()
{
    GpgpuProvider::gpgpu()->writeBuffer(_camera_buffer, _camera.serialize(Serializable::Type::WithAlignment).data());
}