#include <stdexcept>

#include "object.h"


using namespace flat;

using std::logic_error;


Object::~Object()
{ }


bool Object::operator==(const Object& other) const
{
    return (_model_id == other._model_id) && (_obb == other._obb);
}

bool Object::operator!=(const Object& other) const
{
    return (_model_id != other._model_id) || (_obb != other._obb);
}


vector<unsigned char> Object::serialize(Serializable::Type type) const
{
    vector<unsigned char> data(size(type));

    serialize(data.data(), type);

    return data;
}

void Object::serialize(unsigned char* pointer,
                       Serializable::Type type) const
{
    unsigned long long* long_pointer;

    switch (type)
    {
        case Type::MinimalSize:
        case Type::WithAlignment:
            long_pointer = reinterpret_cast<unsigned long long*>(pointer);
            *long_pointer++ = _model_id;
            pointer = reinterpret_cast<unsigned char*>(long_pointer);
            _obb.serialize(pointer, type);
            break;

        default:
            throw logic_error("Unsupported serialization type.");
    }
}

void Object::deserialize(const vector<unsigned char>& data,
                         Serializable::Type type)
{
    if (data.size() != size(type))
        throw logic_error("Size of the vector with data does not comply with requirements for Object deserialization.");

    deserialize(data.data(), type);
}

void Object::deserialize(const unsigned char* pointer,
                         Serializable::Type type)
{
    const unsigned long long* long_pointer;

    switch (type)
    {
        case Type::MinimalSize:
        case Type::WithAlignment:
            long_pointer = reinterpret_cast<const unsigned long long*>(pointer);
            _model_id = *long_pointer++;
            pointer = reinterpret_cast<const unsigned char*>(long_pointer);
            _obb.deserialize(pointer, type);
            break;

        default:
            throw logic_error("Unsupported serialization type.");
    }
}

size_t Object::size(Serializable::Type type) const
{
    switch (type)
    {
        case Type::MinimalSize:
        case Type::WithAlignment:
            return sizeof(_model_id) + _obb.size(type);

        default:
            throw logic_error("Unsupported serialization type.");
    }
}


unsigned long long& Object::model_id()
{
    return _model_id;
}

unsigned long long Object::model_id() const
{
    return _model_id;
}

Obb& Object::obb()
{
    return _obb;
}

const Obb& Object::obb() const
{
    return _obb;
}
