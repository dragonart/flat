#include <stdexcept>

#include "obb.h"


using namespace flat;

using std::logic_error;


Obb::Obb(Vector center,
         float half_size,
         Quaternion quaternion) :
        center(center),
        half_size(half_size),
        quaternion(quaternion)
{ }

Obb::~Obb()
{ }

bool Obb::operator==(const Obb& other) const
{
    return (center == other.center) && (half_size == other.half_size) && (quaternion == other.quaternion);
}

bool Obb::operator!=(const Obb& other) const
{
    return (center != other.center) || (half_size != other.half_size) || (quaternion != other.quaternion);
}


vector<unsigned char> Obb::serialize(Serializable::Type type) const
{
    vector<unsigned char> data(size(type), 0x00);

    serialize(data.data(), type);

    return data;
}

void Obb::serialize(unsigned char* pointer,
                    Serializable::Type type) const
{
    float* float_pointer;
    switch (type)
    {
        case Type::MinimalSize:
            center.serialize(pointer, type);
            pointer += center.size(type);
            float_pointer = reinterpret_cast<float*>(pointer);
            *float_pointer++ = half_size;
            pointer = reinterpret_cast<unsigned char*>(float_pointer);
            quaternion.serialize(pointer, type);
            break;

        case Type::WithAlignment:
            center.serialize(pointer, type);
            pointer += center.size(type);
            float_pointer = reinterpret_cast<float*>(pointer);
            *float_pointer = half_size;
            pointer += sizeof(float) * 4;
            quaternion.serialize(pointer, type);
            break;

        default:
            throw logic_error("Unsupported serialization type.");
    }
}

void Obb::deserialize(const vector<unsigned char>& data,
                      Serializable::Type type)
{
    if (data.size() != size(type))
        throw logic_error("Size of the vector with data does not comply with requirements for Obb deserialization.");

    deserialize(data.data(), type);
}

void Obb::deserialize(const unsigned char* pointer,
                      Serializable::Type type)
{
    const float* float_pointer;

    switch (type)
    {
        case Type::MinimalSize:
            center.deserialize(pointer, type);
            pointer += center.size(type);
            float_pointer = reinterpret_cast<const float*>(pointer);
            half_size = *float_pointer++;
            pointer = reinterpret_cast<const unsigned char*>(float_pointer);
            quaternion.deserialize(pointer, type);
            break;

        case Type::WithAlignment:
            center.deserialize(pointer, type);
            pointer += center.size(type);
            float_pointer = reinterpret_cast<const float*>(pointer);
            half_size = *float_pointer;
            pointer += sizeof(float) * 4;
            quaternion.deserialize(pointer, type);
            break;

        default:
            throw logic_error("Unsupported serialization type.");
    }
}

size_t Obb::size(Serializable::Type type) const
{
    switch (type)
    {
        case Type::MinimalSize:
            return center.size(type) + sizeof(float) + quaternion.size(type);

        case Type::WithAlignment:
            return center.size(type) + sizeof(float) * 4 + quaternion.size(type);

        default:
            throw logic_error("Unsupported serialization type.");
    }
}