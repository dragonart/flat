#pragma once


#include "math/vector.h"
#include "math/quaternion.h"


namespace flat
{
    class Obb : public Serializable
    {
    public:
        Vector center; // TODO: protect members with Vector& quaternion() etc.
        float half_size;
        Quaternion quaternion;


    public:
        Obb(Vector center = Vector(),
            float half_size = 0.5,
            Quaternion quaternion = Quaternion());
        virtual ~Obb();

        bool operator==(const Obb& other) const;
        bool operator!=(const Obb& other) const;

        virtual vector<unsigned char> serialize(Type type) const override;
        virtual void serialize(unsigned char* pointer,
                               Type type) const override;
        virtual void deserialize(const vector<unsigned char>& data,
                                 Type type) override;
        virtual void deserialize(const unsigned char* pointer,
                                 Type type) override;
        virtual size_t size(Type type) const override;
    };
}