#pragma once


#include <vector>

#include "octree.h"


using std::string;
using std::ifstream;
using std::vector;


namespace flat
{
    class Model : public Serializable
    {
    public:
        Model();
        virtual ~Model();

        bool operator==(const Model& other) const;
        bool operator!=(const Model& other) const;

        void load(string file_name);
        void release();

        unsigned long long total_nodes_number() const;
        const vector<Octree>& data() const;

        virtual vector<unsigned char> serialize(Type type) const override;
        virtual void serialize(unsigned char* pointer,
                               Type type) const override;
        virtual void deserialize(const vector<unsigned char>& data,
                                 Type type) override;
        virtual void deserialize(const unsigned char* pointer,
                                 Type type) override;
        virtual size_t size(Type type) const override;


    private:
        enum class LineType : unsigned char
        {
            Unknown,
            LevelHeader,
            Color,
            LeafLocations,
            Comment,
            End
        };


    private:
        vector<Octree> _data;


    private:
        Color readColor(ifstream& file) const;
        LineType readLineType(ifstream& file) const;
        unsigned char readLeafLocations(ifstream& file) const;
        unsigned long long leavesIn(unsigned char& locations) const;
    };
}