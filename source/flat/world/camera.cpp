#include <stdexcept>
#include <iostream>

#include "math/quaternion.h"
#include "camera.h"


using namespace flat;

using std::logic_error;
using std::cout;
using std::endl;


Camera::Camera(Vector position,
               Vector direction,
               Vector up,
               Vector right,
               float fov) :
        _position(position),
        _direction(direction),
        _up(up),
        _right(right),
        _fov(fov)
{ }


int Camera::execute(Command command)
{
    if (command.header() == "move")
    {
        if (command.at(1) == "forwards")
        {
            _position += _direction * Step;
            return 0;
        }
        if (command.at(1) == "backwards")
        {
            _position -= _direction * Step;
            return 0;
        }
        if (command.at(1) == "right")
        {
            _position += _right * Step;
            return 0;
        }
        if (command.at(1) == "left")
        {
            _position -= _right * Step;
            return 0;
        }
    }

    if (command.header() == "rotate")
    {
        if (command.at(1) == "up")
        {
            auto quaternion = Quaternion(_right, RotationDelta);
            _direction = Quaternion::rotate(_direction, quaternion);
            _up = Quaternion::rotate(_up, quaternion);
            return 0;
        }
        if (command.at(1) == "down")
        {
            auto quaternion = Quaternion(_right, -RotationDelta);
            _direction = Quaternion::rotate(_direction, quaternion);
            _up = Quaternion::rotate(_up, quaternion);
            return 0;
        }
        if (command.at(1) == "left")
        {
            auto quaternion = Quaternion(_up, RotationDelta);
            _direction = Quaternion::rotate(_direction, quaternion);
            _right = Quaternion::rotate(_right, quaternion);
            return 0;
        }
        if (command.at(1) == "right")
        {
            auto quaternion = Quaternion(_up, -RotationDelta);
            _direction = Quaternion::rotate(_direction, quaternion);
            _right = Quaternion::rotate(_right, quaternion);
            return 0;
        }
    }

    if (command.header() == "zoom")
    {
        if (command.at(1) == "in")
        {
            _fov *= 0.95f;
            return 0;
        }
        if (command.at(1) == "out")
        {
            _fov *= 1.05f;
            return 0;
        }
    }

    throw logic_error("Unknown command for Camera: " + command.totalString());
}


bool Camera::operator==(const Camera& other) const
{
    return (_position == other._position) && (_direction == other._direction) && (_up == other._up) && (_right == other._right) && (_fov == other._fov);
}

bool Camera::operator!=(const Camera& other) const
{
    return (_position != other._position) || (_direction != other._direction) || (_up != other._up) || (_right != other._right) || (_fov != other._fov);
}


void Camera::setPosition(Vector position)
{
    _position = position;
}

void Camera::setDirection(Vector direction)
{
    _direction = direction;
}

void Camera::setUp(Vector up)
{
    _up = up;
}

void Camera::setRight(Vector right)
{
    _right = right;
}

void Camera::setFov(float fov)
{
    _fov = fov;
}

const Vector& Camera::getPosition() const
{
    return _position;
}

const Vector& Camera::getDirection() const
{
    return _direction;
}

const Vector& Camera::getUp() const
{
    return _up;
}

const Vector& Camera::getRight() const
{
    return _right;
}

float Camera::getFov() const
{
    return _fov;
}

vector<unsigned char> Camera::serialize(Serializable::Type type) const
{
    vector<unsigned char> data(size(type), 0x00);

    serialize(data.data(), type);

    return data;
}

void Camera::serialize(unsigned char* pointer,
                       Serializable::Type type) const
{
    float* float_pointer;
    switch (type)
    {
        case Type::MinimalSize:
        case Type::WithAlignment:
            _position.serialize(pointer, type);
            pointer += _position.size(type);
            _direction.serialize(pointer, type);
            pointer += _direction.size(type);
            _up.serialize(pointer, type);
            pointer += _up.size(type);
            _right.serialize(pointer, type);
            pointer += _right.size(type);

            float_pointer = reinterpret_cast<float*>(pointer);
            *float_pointer = _fov;
            break;

        default:
            throw logic_error("Unsupported serialization type.");
    }
}

void Camera::deserialize(const vector<unsigned char>& data,
                         Serializable::Type type)
{
    if (data.size() != size(type))
        throw logic_error("Size of the vector with data does not comply with requirements for Camera deserialization.");

   deserialize(data.data(), type);
}

void Camera::deserialize(const unsigned char* pointer,
                         Serializable::Type type)
{
    const float* float_pointer;

    switch (type)
    {
        case Type::MinimalSize:
        case Type::WithAlignment:
            _position.deserialize(pointer, type);
            pointer += _position.size(type);
            _direction.deserialize(pointer, type);
            pointer += _direction.size(type);
            _up.deserialize(pointer, type);
            pointer += _up.size(type);
            _right.deserialize(pointer, type);
            pointer += _right.size(type);

            float_pointer = reinterpret_cast<const float*>(pointer);
            _fov = *float_pointer;
            break;

        default:
            throw logic_error("Unsupported serialization type.");
    }
}

size_t Camera::size(Serializable::Type type) const
{
    switch (type)
    {
        case Type::MinimalSize:
            return _position.size(type) + _direction.size(type) + _up.size(type) + _right.size(type) + sizeof(float);

        case Type::WithAlignment:
            return _position.size(type) + _direction.size(type) + _up.size(type) + _right.size(type) + sizeof(float) * 4;

        default:
            throw logic_error("Unsupported serialization type.");
    }
}