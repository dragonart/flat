#include <stdexcept>

#include "octree.h"


using namespace flat;

using std::logic_error;


Octree::Octree(Color color)
{
    this->color = color;
    monolith = true;
    for (auto& leaf : leaves)
        leaf = 0;
}

Octree::~Octree()
{ }


bool Octree::operator==(const Octree& other) const
{
    return (color == other.color) && (monolith == other.monolith) && (leaves[0] == other.leaves[0]) && (leaves[1] == other.leaves[1]) && (leaves[2] == other.leaves[2]) &&
           (leaves[3] == other.leaves[3]) && (leaves[4] == other.leaves[4]) && (leaves[5] == other.leaves[5]) && (leaves[6] == other.leaves[6]) && (leaves[7] == other.leaves[7]);
}

bool Octree::operator!=(const Octree& other) const
{
    return (color != other.color) || (monolith != other.monolith) || (leaves[0] != other.leaves[0]) || (leaves[1] != other.leaves[1]) || (leaves[2] != other.leaves[2]) ||
           (leaves[3] != other.leaves[3]) || (leaves[4] != other.leaves[4]) || (leaves[5] != other.leaves[5]) || (leaves[6] != other.leaves[6]) || (leaves[7] != other.leaves[7]);
}


vector<unsigned char> Octree::serialize(Serializable::Type type) const
{
    vector<unsigned char> data(size(type), 0x00);

    serialize(data.data(), type);

    return data;
}

void Octree::serialize(unsigned char* pointer,
                       Serializable::Type type) const
{
    unsigned long long* long_pointer;

    switch (type)
    {
        case Type::MinimalSize:
            color.serialize(pointer, type);
            pointer += color.size(type);
            *pointer++ = monolith ? static_cast<unsigned char>(0x01) : static_cast<unsigned char>(0x00);
            long_pointer = reinterpret_cast<unsigned long long*>(pointer);
            for (unsigned char i = 0; i < 8; ++i)
                *long_pointer++ = leaves[i];
            break;

        case Type::WithAlignment:
            color.serialize(pointer, type);
            pointer += color.size(type);
            *pointer = monolith ? static_cast<unsigned char>(0x01) : static_cast<unsigned char>(0x00);
            pointer += 4;
            long_pointer = reinterpret_cast<unsigned long long*>(pointer);
            for (unsigned char i = 0; i < 8; ++i)
                *long_pointer++ = leaves[i];
            break;

        default:
            throw logic_error("Unsupported serialization type.");
    }
}

void Octree::deserialize(const vector<unsigned char>& data,
                         Serializable::Type type)
{
    if (data.size() != size(type))
        throw logic_error("Size of the vector with data does not comply with requirements for Octree deserialization.");

    deserialize(data.data(), type);
}

void Octree::deserialize(const unsigned char* pointer,
                         Serializable::Type type)
{
    const unsigned long long* long_pointer;

    switch (type)
    {
        case Type::MinimalSize:
            color.deserialize(pointer, type);
            pointer += color.size(type);
            monolith = *pointer++ == static_cast<unsigned char>(0x01);
            long_pointer = reinterpret_cast<const unsigned long long*>(pointer);
            for (unsigned char i = 0; i < 8; ++i)
                leaves[i] = *long_pointer++;
            break;

        case Type::WithAlignment:
            color.deserialize(pointer, type);
            pointer += color.size(type);
            monolith = *pointer == static_cast<unsigned char>(0x01);
            pointer += 4;
            long_pointer = reinterpret_cast<const unsigned long long*>(pointer);
            for (unsigned char i = 0; i < 8; ++i)
                leaves[i] = *long_pointer++;
            break;

        default:
            throw logic_error("Unsupported serialization type.");
    }
}

size_t Octree::size(Serializable::Type type) const
{
    switch (type)
    {
        case Type::MinimalSize:
            return color.size(type) + 1 + sizeof(unsigned long long) * 8;
        case Type::WithAlignment:
            return color.size(type) + 4 + sizeof(unsigned long long) * 8;

        default:
            throw logic_error("Unsupported serialization type.");
    }
}