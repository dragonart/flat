#include <stdexcept>
#include <fstream>
#include <iostream>
#include <iomanip>

#include "model.h"


using namespace flat;

using std::logic_error;
using std::runtime_error;
using std::ifstream;
using std::cout;
using std::endl;


Model::Model()
{ }

Model::~Model()
{ }


bool Model::operator==(const Model& other) const
{
    if (_data.size() != other._data.size())
        return false;

    for (size_t i = 0; i < _data.size(); ++i)
        if (_data[i] != other._data[i])
            return false;

    return true;
}

bool Model::operator!=(const Model& other) const
{
    return !(*this == other);
}


void Model::load(string file_name)
{
    ifstream file;
    file.open(file_name);
    if (!file.is_open())
        throw runtime_error("Model loading from file '" + file_name + "' failed: failed to open file.");
    cout << "Loading model from file '" << file_name << "':" << endl;

    string line;
    file >> line >> line;
    cout << "Name: " << line << endl;
    unsigned short number;
    file >> line >> number;
    cout << "Depth: " << number << endl;
    unsigned char depth = number;
    unsigned long long total_nodes_number;
    file >> line >> total_nodes_number;
    cout << "Total nodes number: " << total_nodes_number << endl;
    cout << "Octree:" << endl;

    Octree empty_node(Color(0, 0, 0, 0));
    _data = vector<Octree>(total_nodes_number, empty_node);
    vector<unsigned char> leaf_locations(total_nodes_number, 0);
    bool leaf_exists;
    unsigned long long current_index = 0;
    unsigned long long current_level_start = 0;
    unsigned long long current_level_size = 1;
    unsigned long long next_level_size = 0;
    unsigned long long current_level_nodes_loaded = 0;
    unsigned long long leaf_index;
    LineType line_type = readLineType(file);

    for (unsigned char level = 0; level < depth; ++level)
    {
        file >> line;
        cout << "  " << static_cast<unsigned short>(level) << ": " << static_cast<unsigned short>(current_level_size) << endl;
        line_type = readLineType(file);

        while (current_level_nodes_loaded != current_level_size)
        {
            if (line_type == LineType::Color)
            {
                _data[current_index].color = readColor(file);
                cout << "    " << _data[current_index].color.toString() << endl;
                current_index++;
                current_level_nodes_loaded++;
                line_type = readLineType(file);
            }
            if (line_type == LineType::LeafLocations)
            {
                _data[current_index - 1].monolith = false;
                leaf_locations[current_index - 1] = readLeafLocations(file);
                next_level_size += leavesIn(leaf_locations[current_index - 1]);
                line_type = readLineType(file);
            }
            if (line_type == LineType::End)
                break;
            if (line_type == LineType::Comment)
                do
                {
                    line_type = readLineType(file);
                }
                while (line_type != LineType::Color);
        }

        if (line_type != LineType::End)
        {
            leaf_index = 0;

            for (unsigned long long i = current_level_start; i < current_index; ++i)
            {
                if (_data[i].monolith)
                    continue;

                for (unsigned char j = 0; j < 8; ++j)
                    if (leaf_locations[i] & (1 << j))
                    {
                        _data[i].leaves[j] = current_index + leaf_index;
                        leaf_index++;
                    }
            }

            current_level_start = current_index;
            current_level_size = next_level_size;
            current_level_nodes_loaded = 0;
            next_level_size = 0;
        }
    }

    file.close();
    cout << "Loading model from file '" << file_name << "' succeeded." << endl;
}

void Model::release()
{
    cout << "Model release succeeded." << endl;
}

unsigned long long Model::total_nodes_number() const
{
    return _data.size();
}

const vector<Octree>& Model::data() const
{
    return _data;
}


vector<unsigned char> Model::serialize(Serializable::Type type) const
{
    vector<unsigned char> data(size(type), 0x00);

    serialize(data.data(), type);

    return data;
}

void Model::serialize(unsigned char* pointer,
                      Serializable::Type type) const
{
    auto size_pointer = reinterpret_cast<size_t*>(pointer);

    switch (type)
    {
        case Type::MinimalSize:
        case Type::WithAlignment:
            *size_pointer++ = _data.size();
            pointer = reinterpret_cast<unsigned char*>(size_pointer);
            for (size_t i = 0; i < _data.size(); ++i)
            {
                _data[i].serialize(pointer, type);
                pointer += _data[0].size(type);
            }
            break;

        default:
            throw logic_error("Unsupported serialization type.");
    }
}

void Model::deserialize(const vector<unsigned char>& data,
                        Serializable::Type type)
{
    Octree typical_octree;

    auto size_pointer = reinterpret_cast<const size_t*>(data.data());
    auto needed_size = *size_pointer * typical_octree.size(type) + sizeof(size_t);
    if (data.size() != needed_size)
        throw logic_error("Size of the vector with data does not comply with requirements for Model deserialization.");

    auto pointer = data.data();
    deserialize(pointer, type);
}

void Model::deserialize(const unsigned char* pointer,
                        Serializable::Type type)
{
    auto size_pointer = reinterpret_cast<const size_t*>(pointer);

    switch (type)
    {
        case Type::MinimalSize:
        case Type::WithAlignment:
            _data = vector<Octree>(*size_pointer++);
            pointer = reinterpret_cast<const unsigned char*>(size_pointer);
            for (size_t i = 0; i < _data.size(); ++i)
            {
                _data[i].deserialize(pointer, type);
                pointer += _data[0].size(type);
            }
            break;

        default:
            throw logic_error("Unsupported serialization type.");
    }
}

size_t Model::size(Serializable::Type type) const
{
    Octree typical_octree;

    switch (type)
    {
        case Type::MinimalSize:
        case Type::WithAlignment:
            return sizeof(size_t) + typical_octree.size(type) * _data.size();

        default:
            throw logic_error("Unsupported serialization type.");
    }
}


Color Model::readColor(ifstream& file) const
{
    unsigned short number;
    Color color;

    file >> number;
    color.red = number;

    file >> number;
    color.green = number;

    file >> number;
    color.blue = number;

    return color;
}

Model::LineType Model::readLineType(ifstream& file) const
{
    string line;
    file >> line;

    if (line == "c")
        return LineType::Color;
    if (line == "l")
        return LineType::LeafLocations;
    if (line == ".level")
        return LineType::LevelHeader;
    if (line == "end")
        return LineType::End;
    if (line == "#")
        return LineType::Comment;

    return LineType::Unknown;
}

unsigned char Model::readLeafLocations(ifstream& file) const
{
    bool leaf_exists;
    unsigned char locations = 0;

    for (unsigned char i = 0; i < 8; ++i)
    {
        file >> leaf_exists;
        locations += leaf_exists << i;
    }

    return locations;
}

unsigned long long Model::leavesIn(unsigned char& locations) const
{
    unsigned char leaves_number = 0;

    for (unsigned char i = 0; i < 8; ++i)
        if (locations & (1 << i))
            leaves_number++;

    return leaves_number;
}
