#pragma once


#include "model.h"
#include "obb.h"


namespace flat
{
    class Object : public Serializable
    {
    public:
        virtual ~Object();

        bool operator==(const Object& other) const;
        bool operator!=(const Object& other) const;

        virtual vector<unsigned char> serialize(Type type) const override;
        virtual void serialize(unsigned char* pointer,
                               Type type) const override;
        virtual void deserialize(const vector<unsigned char>& data,
                                 Type type) override;
        virtual void deserialize(const unsigned char* pointer,
                                 Type type) override;
        virtual size_t size(Type type) const override;

        unsigned long long& model_id();
        unsigned long long model_id() const;
        Obb& obb();
        const Obb& obb() const;


    private:
        unsigned long long _model_id;
        Obb _obb;
    };
}