#pragma once


#include <string>


using std::string;


namespace flat
{
    class Lexeme
    {
    public:
        explicit Lexeme(string line);
        explicit Lexeme(const char* line);
        explicit Lexeme(void* pointer);
        explicit Lexeme(double value);
        explicit Lexeme(int number);
        explicit Lexeme(unsigned int number);

        const string& toString() const;
        void* toPointer() const;
        int toInt() const;
        unsigned int toUnsignedInt() const;
        double toDouble() const;

        bool operator==(const string& line) const;


    private:
        string _value;
    };
}