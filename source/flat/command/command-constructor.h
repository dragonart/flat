#pragma once


#include "command.h"


namespace flat
{
    class CommandConstructor
    {
    public:
        template<class ...Types>
        static Command construct(Types ...lexemes)
        {
            Command command;
            CommandConstructor::addLexemes(&command, lexemes...);
            return command;
        }

        template<class T>
        static void addLexemes(Command* command, const T& lexeme)
        {
            command->_lexemes.push_back(Lexeme(lexeme));
        }

        template<class T, class ...Types>
        static void addLexemes(Command* command, const T& lexeme, const Types& ...lexemes)
        {
            command->_lexemes.push_back(Lexeme(lexeme));
            addLexemes(command, lexemes...);
        }

        template<class ...Types>
        static void addLexemes(Command* command, const Types& ...lexemes)
        {
            addLexemes(command, lexemes...);
        }
    };
}