#pragma once


#include "command.h"


namespace flat
{
    class Executor
    {
    public:
        virtual ~Executor()
        { }

        virtual int execute(Command command) = 0;
    };
}