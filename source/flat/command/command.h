#pragma once


#include <vector>

#include "lexeme.h"


using std::vector;


namespace flat
{
    class Command
    {
        friend class CommandConstructor;


    public:
        Command();
        Command(const Command& other);

        const Command& operator=(const Command& other);

        string totalString() const;

        bool isEmpty() const;
        string header() const;
        const Lexeme& at(unsigned int index) const;

        Command withoutHeader() const;


    private:
        vector<Lexeme> _lexemes;
    };
}