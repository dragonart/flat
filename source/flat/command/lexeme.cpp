#include <sstream>

#include "lexeme.h"


using namespace flat;

using std::stringstream;


Lexeme::Lexeme(string line)
{
    _value = line;
}

Lexeme::Lexeme(const char* line)
{
    _value = line;
}

Lexeme::Lexeme(void* pointer)
{
    stringstream stream;
    stream << pointer;
    _value = stream.str();
}

Lexeme::Lexeme(double value)
{
    stringstream stream;
    stream << value;
    _value = stream.str();
}

Lexeme::Lexeme(int number)
{
    stringstream stream;
    stream << number;
    _value = stream.str();
}

Lexeme::Lexeme(unsigned int number)
{
    stringstream stream;
    stream << number;
    _value = stream.str();
}


const string& Lexeme::toString() const
{
    return _value;
}

void* Lexeme::toPointer() const
{
    stringstream stream(_value);
    void* pointer;

    stream >> pointer;

    return pointer;
}

double Lexeme::toDouble() const
{
    stringstream stream(_value);
    double value;

    stream >> value;

    return value;
}

int Lexeme::toInt() const
{
    stringstream stream(_value);
    int number;

    stream >> number;

    return number;
}

unsigned int Lexeme::toUnsignedInt() const
{
    stringstream stream(_value);
    unsigned int number;

    stream >> number;

    return number;
}

bool Lexeme::operator==(const string& line) const
{
    return _value == line;
}
