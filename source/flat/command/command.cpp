#include <stdexcept>

#include "command.h"
#include "command-constructor.h"


using namespace flat;

using std::logic_error;
using std::to_string;


Command::Command()
{ }

Command::Command(const Command& other) :
        _lexemes(other._lexemes)
{ }

const Command& Command::operator=(const Command& other)
{
    _lexemes = other._lexemes;
    return *this;
}


bool Command::isEmpty() const
{
    return _lexemes.size() == 0;
}

string Command::header() const
{
    if (_lexemes.size() == 0)
        //throw logic_error("Command is empty.");
        return "";
    return _lexemes[0].toString();
}

const Lexeme& Command::at(unsigned int index) const
{
    if (_lexemes.size() <= index)
        throw logic_error("Command does NOT contain lexeme #" + to_string(index) + ".");

    return _lexemes[index];
}

Command Command::withoutHeader() const
{
    Command command;

    for (int i = 1; i < _lexemes.size(); ++i)
        command._lexemes.push_back(_lexemes[i]);

    return command;
}

string Command::totalString() const
{
    string line;

    for (auto& lexeme : _lexemes)
        line += lexeme.toString() + " ";

    return line;
}
