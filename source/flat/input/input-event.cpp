#include "input-event.h"


using namespace flat;


InputEvent::InputEvent(Group group) :
        _group(group)
{ }

InputEvent::Group InputEvent::group() const
{
    return _group;
}