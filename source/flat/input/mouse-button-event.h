#pragma once


#include "mouse-event.h"
#include "button-action.h"
#include "mouse-state.h"


class GLFWwindow;


namespace flat
{
    class MouseButtonEvent : public MouseEvent
    {
    public:
        MouseButtonEvent(GLFWwindow* window,
                         int button,
                         int action,
                         int modificators);

        GLFWwindow* window() const;
        MouseState::ButtonCode button() const;
        ButtonAction action() const;
        int modificators() const;


    private:
        GLFWwindow* _window;
        MouseState::ButtonCode _button;
        ButtonAction _action;
        int _modificators;
    };
}