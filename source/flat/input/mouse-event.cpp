#include "mouse-event.h"


using namespace flat;


MouseEvent::MouseEvent(MouseEvent::Type type) :
        InputEvent(InputEvent::Group::Mouse),
        _type(type)
{ }

MouseEvent::Type MouseEvent::type() const
{
    return _type;
}
