#pragma once


namespace flat
{
    enum class ButtonAction : unsigned char
    {
        Press,
        Release,
        Repeat
    };
}