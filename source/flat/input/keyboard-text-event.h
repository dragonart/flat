#pragma once


#include "keyboard-event.h"


class GLFWwindow;


namespace flat
{
    class KeyboardTextEvent : public KeyboardEvent
    {
    public:
        KeyboardTextEvent(GLFWwindow* window,
                          unsigned int codepoint,
                          int modificators);

        GLFWwindow* window() const;
        unsigned int codepoint() const;
        int modificators() const;


    private:
        GLFWwindow* _window;
        unsigned int _codepoint;
        int _modificators;
    };
}