#pragma once


#include "keyboard-event.h"
#include "button-action.h"
#include "keyboard-state.h"


class GLFWwindow;


namespace flat
{
    class KeyboardKeyEvent : public KeyboardEvent
    {
    public:
        KeyboardKeyEvent(GLFWwindow* window,
                         int key,
                         int scancode,
                         int action,
                         int modificators);

        GLFWwindow* window() const;
        KeyboardState::KeyCode key() const;
        int scancode() const;
        ButtonAction action() const;
        int modificators() const;


    private:
        GLFWwindow* _window;
        KeyboardState::KeyCode _key;
        int _scancode;
        ButtonAction _action;
        int _modificators;
    };
}