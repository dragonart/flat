#pragma once


#include "window-event.h"


class GLFWwindow;


namespace flat
{
    class WindowCloseEvent : public WindowEvent
    {
    public:
        WindowCloseEvent(GLFWwindow* window);

        GLFWwindow* window() const;


    private:
        GLFWwindow* _window;
    };
}