#include <GLFW/glfw3.h>

#include "mouse-button-event.h"


using namespace flat;

using std::logic_error;


MouseButtonEvent::MouseButtonEvent(GLFWwindow* window,
                                   int button,
                                   int action,
                                   int modificators) :
        MouseEvent(MouseEvent::Type::Button)
{
    _window = window;

    switch(button)
    {
        case GLFW_MOUSE_BUTTON_LEFT:
            _button = MouseState::ButtonCode::Left;
            break;

        case GLFW_MOUSE_BUTTON_RIGHT:
            _button = MouseState::ButtonCode::Right;
            break;

        case GLFW_MOUSE_BUTTON_MIDDLE:
            _button = MouseState::ButtonCode::Middle;
            break;

        default:
            throw logic_error("Only three basic mouse buttons are processing correct just now.");
    }

    switch(action)
    {
        case GLFW_PRESS:
            _action = ButtonAction::Press;
            break;

        case GLFW_REPEAT:
            _action = ButtonAction::Repeat;
            break;

        case GLFW_RELEASE:
            _action = ButtonAction::Release;
            break;

        default:
            throw logic_error("Unknown mouse button action.");
    }

    _modificators = modificators;
}

GLFWwindow* MouseButtonEvent::window() const
{
    return _window;
}

MouseState::ButtonCode MouseButtonEvent::button() const
{
    return _button;
}

ButtonAction MouseButtonEvent::action() const
{
    return _action;
}

int MouseButtonEvent::modificators() const
{
    return _modificators;
}
