#include "window-close-event.h"


using namespace flat;


WindowCloseEvent::WindowCloseEvent(GLFWwindow* window) :
        WindowEvent(WindowEvent::Type::Close)
{
    _window = window;
}

GLFWwindow* WindowCloseEvent::window() const
{
    return _window;
}
