#pragma once


#include <map>
#include <vector>
#include <memory>

#include "button-state.h"
#include "mouse-event.h"


using std::map;
using std::vector;
using std::shared_ptr;


namespace flat
{
    class MouseState
    {
    public:
        enum class ButtonCode : unsigned char
        {
            Left,
            Right,
            Middle
        };


    public:
        ButtonState& button(ButtonCode code);

        double x() const;
        double y() const;

        void applyEvent(MouseEventPointer event);


    private:
        map<ButtonCode, ButtonState> _buttons;
        double _x, _y;
    };
}