#pragma once


#include <mutex>

#include <GLFW/glfw3.h>

#include "keyboard-state.h"
#include "mouse-state.h"
#include "input-event.h"


using std::mutex;
using std::unique_lock;


namespace flat
{
    class Input
    {
    public:
        void initialize(GLFWwindow* window);
        void release();

        void addEvent(InputEventPointer event);
        void applyEvents();

        KeyboardState& keyboard();
        MouseState& mouse();
        shared_ptr<vector<InputEventPointer>> events();


    private:
        static const size_t OptimizedEventsStackSize = 10;


    private:
        KeyboardState _keyboard;
        MouseState _mouse;

        shared_ptr<vector<InputEventPointer>> _events;

        unique_lock<mutex> _lock;
        mutex _mutex;
    };
}
