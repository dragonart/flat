#include "window-event.h"


using namespace flat;


WindowEvent::WindowEvent(Type type) :
        InputEvent(InputEvent::Group::Window),
        _type(type)
{ }

WindowEvent::Type WindowEvent::type() const
{
    return _type;
}