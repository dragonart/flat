#include <stdexcept>

#include <GLFW/glfw3.h>

#include "keyboard-key-event.h"


using namespace flat;

using std::logic_error;
using std::to_string;


KeyboardKeyEvent::KeyboardKeyEvent(GLFWwindow* window,
                                   int key,
                                   int scancode,
                                   int action,
                                   int modificators) :
        KeyboardEvent(KeyboardEvent::Type::Key)
{
    _window = window;
    _scancode = scancode;
    _modificators = modificators;

    switch (key)
    {
        case GLFW_KEY_W:
            _key = KeyboardState::KeyCode::W;
            break;

        case GLFW_KEY_A:
            _key = KeyboardState::KeyCode::A;
            break;

        case GLFW_KEY_S:
            _key = KeyboardState::KeyCode::S;
            break;

        case GLFW_KEY_D:
            _key = KeyboardState::KeyCode::D;
            break;

        case GLFW_KEY_LEFT:
            _key = KeyboardState::KeyCode::ArrowLeft;
            break;

        case GLFW_KEY_RIGHT:
            _key = KeyboardState::KeyCode::ArrowRight;
            break;

        case GLFW_KEY_UP:
            _key = KeyboardState::KeyCode::ArrowUp;
            break;

        case GLFW_KEY_DOWN:
            _key = KeyboardState::KeyCode::ArrowDown;
            break;

        case GLFW_KEY_ESCAPE:
            _key = KeyboardState::KeyCode::Escape;
            break;

        default:
            _key = KeyboardState::KeyCode::Unknown;
            break;
    }

    switch (action)
    {
        case GLFW_PRESS:
            _action = ButtonAction::Press;
            break;

        case GLFW_REPEAT:
            _action = ButtonAction::Repeat;
            break;

        case GLFW_RELEASE:
            _action = ButtonAction::Release;
            break;

        default:
            throw logic_error("Keyboard action #" + to_string(action) + " is not implemented.");
    }
}

GLFWwindow* KeyboardKeyEvent::window() const
{
    return _window;
}

KeyboardState::KeyCode KeyboardKeyEvent::key() const
{
    return _key;
}

int KeyboardKeyEvent::scancode() const
{
    return _scancode;
}

ButtonAction KeyboardKeyEvent::action() const
{
    return _action;
}

int KeyboardKeyEvent::modificators() const
{
    return _modificators;
}
