#pragma once


#include "input-event.h"


namespace flat
{
    class KeyboardEvent : public InputEvent
    {
    public:
        enum class Type : unsigned char
        {
            Key,
            Text,
            Unknown
        };


    public:
        virtual ~KeyboardEvent() {}

        Type type() const;


    protected:
        KeyboardEvent(Type type);


    protected:
        Type _type;
    };


    using KeyboardEventPointer = shared_ptr<KeyboardEvent>;
}