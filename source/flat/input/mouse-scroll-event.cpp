#include "mouse-scroll-event.h"


using namespace flat;


MouseScrollEvent::MouseScrollEvent(
        GLFWwindow* window,
        double offset_x,
        double offset_y) :
        MouseEvent(MouseEvent::Type::Scroll)
{
    _window = window;
    _offset_x = offset_x;
    _offset_y = offset_y;
}

GLFWwindow* MouseScrollEvent::window() const
{
    return _window;
}

double MouseScrollEvent::offset_x() const
{
    return _offset_x;
}

double MouseScrollEvent::offset_y() const
{
    return _offset_y;
}
