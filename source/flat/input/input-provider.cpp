#include "input-provider.h"


using namespace flat;

using std::make_shared;


shared_ptr<Input> InputProvider::input()
{
    static auto input_ptr = std::make_shared<Input>();
    return input_ptr;
}
