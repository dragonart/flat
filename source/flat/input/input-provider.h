#pragma once


#include <memory>

#include "input.h"

using std::shared_ptr;


namespace flat
{
    class InputProvider
    {
    public:
        static shared_ptr<Input> input();
    };
}