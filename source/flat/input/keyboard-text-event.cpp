#include "keyboard-text-event.h"


using namespace flat;


KeyboardTextEvent::KeyboardTextEvent(GLFWwindow* window,
                                     unsigned int codepoint,
                                     int modificators) :
        KeyboardEvent(KeyboardEvent::Type::Text)
{
    _window = window;
    _codepoint = codepoint;
    _modificators = modificators;
}

GLFWwindow* KeyboardTextEvent::window() const
{
    return _window;
}

unsigned int KeyboardTextEvent::codepoint() const
{
    return _codepoint;
}

int KeyboardTextEvent::modificators() const
{
    return _modificators;
}
