#include "mouse-state.h"
#include "mouse-button-event.h"
#include "mouse-move-event.h"


using namespace flat;

using std::logic_error;
using std::static_pointer_cast;


ButtonState& MouseState::button(MouseState::ButtonCode code)
{
    return _buttons[code];
}

double MouseState::x() const
{
    return _x;
}

double MouseState::y() const
{
    return _y;
}

void MouseState::applyEvent(MouseEventPointer event)
{
    if(event->type() == MouseEvent::Type::Button)
    {
        auto button_event = static_pointer_cast<MouseButtonEvent>(event);

        switch(button_event->action())
        {
            case ButtonAction::Press:
            case ButtonAction::Repeat:
                _buttons[button_event->button()] = ButtonState::Pressed;
                break;

            case ButtonAction::Release:
                _buttons[button_event->button()] = ButtonState::Released;
                break;

            default:
                throw logic_error("Unknown mouse button action.");
        }

        return;
    }

    if(event->type() == MouseEvent::Type::Move)
    {
        auto move_event = static_pointer_cast<MouseMoveEvent>(event);

        _x += move_event->deltaX();
        _y += move_event->deltaY();

        return;
    }

    if(event->type() == MouseEvent::Type::Scroll)
    {
        // do nothing - mouse do not store any info about scrolling
        return;
    }

    throw logic_error("Unkown mouse event type.");
}