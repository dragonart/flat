#include "keyboard-event.h"


using namespace flat;


KeyboardEvent::KeyboardEvent(KeyboardEvent::Type type) :
        InputEvent(InputEvent::Group::Keyboard),
        _type(type)
{ }

KeyboardEvent::Type KeyboardEvent::type() const
{
    return _type;
}
