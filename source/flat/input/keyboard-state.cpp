#include "keyboard-state.h"
#include "keyboard-key-event.h"


using namespace flat;

using std::logic_error;
using std::static_pointer_cast;


ButtonState& KeyboardState::key(KeyboardState::KeyCode code)
{
    return _keys[code];
}

void KeyboardState::applyEvent(KeyboardEventPointer event)
{
    if(event->type() == KeyboardEvent::Type::Key)
    {
        auto key_event = static_pointer_cast<KeyboardKeyEvent>(event);

        switch(key_event->action())
        {
            case ButtonAction::Press:
            case ButtonAction::Repeat:
                _keys[key_event->key()] = ButtonState::Pressed;
                break;

            case ButtonAction::Release:
                _keys[key_event->key()] = ButtonState::Released;
                break;

            default:
                throw logic_error("Unknown button action.");
        }

        return;
    }

    if(event->type() == KeyboardEvent::Type::Text)
    {
        // there is no need to process text events by keyboard,
        // because every text input causes key pressing too
        return;
    }

    throw logic_error("Unknown keyboard event type.");
}