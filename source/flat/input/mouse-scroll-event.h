#pragma once


#include "mouse-event.h"


class GLFWwindow;


namespace flat
{
    class MouseScrollEvent : public MouseEvent
    {
    public:
        MouseScrollEvent(GLFWwindow* window,
                         double offset_x,
                         double offset_y);

        GLFWwindow* window() const;
        double offset_x() const;
        double offset_y() const;


    private:
        GLFWwindow* _window;
        double _offset_x;
        double _offset_y;
    };
}