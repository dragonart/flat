#pragma once


#include <memory>


using std::shared_ptr;


namespace flat
{
    class InputEvent
    {
    public:
        enum class Group : unsigned char
        {
            Keyboard,
            Mouse,
            Window,
            Unknown
        };


    public:
        virtual ~InputEvent() {}

        Group group() const;


    protected:
        InputEvent(Group group);


    protected:
        Group _group;
    };


    using InputEventPointer = shared_ptr<InputEvent>;
}