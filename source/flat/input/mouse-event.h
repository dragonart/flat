#pragma once


#include "input-event.h"


namespace flat
{
    class MouseEvent : public InputEvent
    {
    public:
        enum class Type : unsigned char
        {
            Move,
            Button,
            Scroll,
            Unknown
        };


    public:
        virtual ~MouseEvent() {}

        Type type() const;


    protected:
        MouseEvent(Type type);


    private:
        Type _type;
    };


    using MouseEventPointer = shared_ptr<MouseEvent>;
}