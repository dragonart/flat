#pragma once


#include <map>
#include <vector>
#include <memory>

#include "button-state.h"
#include "keyboard-event.h"


using std::map;
using std::vector;
using std::shared_ptr;


namespace flat
{
    class KeyboardState
    {
    public:
        enum class KeyCode : unsigned short
        {
            W,
            A,
            S,
            D,
            ArrowUp,
            ArrowDown,
            ArrowLeft,
            ArrowRight,
            Escape,
            Unknown
        };


    public:
        ButtonState& key(KeyCode code);

        void applyEvent(KeyboardEventPointer event);


    private:
        map<KeyCode, ButtonState> _keys;
    };
}