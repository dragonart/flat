#include "input-provider.h"
#include "mouse-move-event.h"


using namespace flat;


MouseMoveEvent::MouseMoveEvent(GLFWwindow* window,
                               double x,
                               double y) :
        MouseEvent(MouseEvent::Type::Move)
{
    _window = window;

    _delta_x = x - InputProvider::input()->mouse().x();
    _delta_y = y - InputProvider::input()->mouse().y();
}

GLFWwindow* MouseMoveEvent::window() const
{
    return _window;
}

double MouseMoveEvent::deltaX() const
{
    return _delta_x;
}

double MouseMoveEvent::deltaY() const
{
    return _delta_y;
}
