#pragma once


#include "mouse-event.h"


class GLFWwindow;


namespace flat
{
    class MouseMoveEvent : public MouseEvent
    {
    public:
        MouseMoveEvent(GLFWwindow* window,
                       double x,
                       double y);

        GLFWwindow* window() const;
        double deltaX() const;
        double deltaY() const;


    private:
        GLFWwindow* _window;
        double _delta_x;
        double _delta_y;
    };
}