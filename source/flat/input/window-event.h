#pragma once


#include "input-event.h"


namespace flat
{
    class WindowEvent : public InputEvent
    {
    public:
        enum class Type : unsigned char
        {
            Close,
            Unknown
        };


    public:
        virtual ~WindowEvent() {}

        Type type() const;


    protected:
        WindowEvent(Type type);


    private:
        Type _type;
    };


    using WindowEventPointer = shared_ptr<WindowEvent>;
}