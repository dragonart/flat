#include <iostream>

#include <GLFW/glfw3.h>

#include "command/command-constructor.h"

#include "input-provider.h"
#include "keyboard-key-event.h"
#include "keyboard-text-event.h"
#include "mouse-button-event.h"
#include "mouse-move-event.h"
#include "window-close-event.h"
#include "mouse-scroll-event.h"


using namespace flat;

using std::cout;
using std::endl;
using std::logic_error;
using std::to_string;
using std::make_shared;
using std::static_pointer_cast;


void Input::initialize(GLFWwindow* window)
{
    _lock = unique_lock<mutex>(_mutex);

    _events = make_shared<vector<InputEventPointer>>();
    _events->reserve(OptimizedEventsStackSize);

    auto keyboard_processor = [](GLFWwindow* window, int key, int scancode, int action, int modificators)
    {
        InputProvider::input()->addEvent(make_shared<KeyboardKeyEvent>(window, key, scancode, action, modificators));
    };
    glfwSetKeyCallback(window, keyboard_processor);

    auto text_processor = [](GLFWwindow* window, unsigned int codepoint, int modificators)
    {
        InputProvider::input()->addEvent(make_shared<KeyboardTextEvent>(window, codepoint, modificators));
    };
    glfwSetCharModsCallback(window, text_processor);


    auto mouse_button_processor = [](GLFWwindow* window, int button, int action, int modificators)
    {
        InputProvider::input()->addEvent(make_shared<MouseButtonEvent>(window, button, action, modificators));
    };
    glfwSetMouseButtonCallback(window, mouse_button_processor);

    auto mouse_move_processor = [](GLFWwindow* window, double x, double y)
    {
        InputProvider::input()->addEvent(make_shared<MouseMoveEvent>(window, x, y));
    };
    glfwSetCursorPosCallback(window, mouse_move_processor);

    auto mouse_scroll_processor = [](GLFWwindow* window, double offset_x, double offset_y)
    {
        InputProvider::input()->addEvent(make_shared<MouseScrollEvent>(window, offset_x, offset_y));
    };
    glfwSetScrollCallback(window, mouse_scroll_processor);

//    auto cursor_enter_processor = [](GLFWwindow* window, int entered)
//    {
//        Engine::execute(CommandConstructor::construct("input", "cursor enter", reinterpret_cast<void*>(window), entered));
//    };
//    glfwSetCursorEnterCallback(window, cursor_enter_processor);
//
//
//    auto path_drop_processor = [](GLFWwindow* window, int count, const char** paths)
//    {
//        Engine::execute(CommandConstructor::construct("input", "path drop", reinterpret_cast<void*>(window), count, reinterpret_cast<void*>(paths)));
//    };
//    glfwSetDropCallback(window, path_drop_processor);
//
//
    auto window_close_processor = [](GLFWwindow* window)
    {
        InputProvider::input()->addEvent(make_shared<WindowCloseEvent>(window));
    };
    glfwSetWindowCloseCallback(window, window_close_processor);
//
//    auto window_resize_processor = [](GLFWwindow* window, int width, int height)
//    {
//        Engine::execute(CommandConstructor::construct("input", "window resize", reinterpret_cast<void*>(window), width, height));
//    };
//    glfwSetWindowSizeCallback(window, window_resize_processor);
//
//    auto window_iconify_processor = [](GLFWwindow* window, int iconified)
//    {
//        Engine::execute(CommandConstructor::construct("input", "window iconify", reinterpret_cast<void*>(window), iconified));
//    };
//    glfwSetWindowIconifyCallback(window, window_iconify_processor);
//
//    auto window_focus_callback = [](GLFWwindow* window, int focused)
//    {
//        Engine::execute(CommandConstructor::construct("input", "window focus", reinterpret_cast<void*>(window), focused));
//    };
//    glfwSetWindowFocusCallback(window, window_focus_callback);
//
//    auto window_refresh_processor = [](GLFWwindow* window)
//    {
//        Engine::execute(CommandConstructor::construct("input", "window refresh", reinterpret_cast<void*>(window)));
//    };
//    glfwSetWindowRefreshCallback(window, window_refresh_processor);
//
//
//    auto monitor_connect_processor = [](GLFWmonitor* monitor, int event)
//    {
//        Engine::execute(CommandConstructor::construct("input", "monitor connect", reinterpret_cast<void*>(monitor), event));
//    };
//    glfwSetMonitorCallback(monitor_connect_processor);

    _lock.unlock();

    cout << "Input initialization succeeded." << endl;
}

void Input::release()
{
    cout << "Input release succeeded." << endl;
}


void Input::addEvent(InputEventPointer event)
{
    _lock.lock();

    _events->push_back(event);

    _lock.unlock();
}

void Input::applyEvents()
{
    for (auto& event : *_events)
    {
        switch (event->group())
        {
            case InputEvent::Group::Keyboard:
                _keyboard.applyEvent(static_pointer_cast<KeyboardEvent>(event));
                break;

            case InputEvent::Group::Mouse:
                _mouse.applyEvent(static_pointer_cast<MouseEvent>(event));
                break;

            default:
                throw logic_error("Only keyboard and mouse events are applying correct to input just now.");
        }
    }

    _events.reset();
    _events = make_shared<vector<InputEventPointer>>();
    _events->reserve(OptimizedEventsStackSize);

    _lock.unlock();
}

KeyboardState& Input::keyboard()
{
    return _keyboard;
}

MouseState& Input::mouse()
{
    return _mouse;
}

shared_ptr<vector<InputEventPointer>> Input::events()
{
    _lock.lock();
    return _events;
}