#pragma once


namespace flat
{
    enum class ButtonState : unsigned char
    {
        Released,
        Pressed
    };
}