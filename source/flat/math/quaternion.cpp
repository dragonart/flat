#include <math.h>
#include <stdexcept>

#include "quaternion.h"
#include "vector.h"


using namespace flat;

using std::logic_error;


Quaternion::Quaternion(float x,
                       float y,
                       float z,
                       float w) :
        x(x), y(y), z(z), w(w)
{ }

Quaternion::Quaternion(const Vector& axis,
                       float angle)
{
    float sine = sinf(angle / 2.0f);
    float cosine = cosf(angle / 2.0f);

    x = axis.x * sine;
    y = axis.y * sine;
    z = axis.z * sine;
    w = cosine;
}

Quaternion::Quaternion(const Quaternion& other) :
        x(other.x), y(other.y), z(other.z), w(other.w)
{ }

const Quaternion& Quaternion::operator=(const Quaternion& other)
{
    x = other.x;
    y = other.y;
    z = other.z;
    w = other.w;

    return *this;
}

bool Quaternion::operator==(const Quaternion& other) const
{
    return (x == other.x) && (y == other.y) && (z == other.z) && (w == other.w);
}

bool Quaternion::operator!=(const Quaternion& other) const
{
    return (x != other.x) || (y != other.y) || (z != other.z) || (w != other.w);
}

Quaternion Quaternion::operator+(const Quaternion& other) const
{
    return Quaternion(x + other.x, y + other.y, z + other.z, w + other.w);
}

const Quaternion& Quaternion::operator+=(const Quaternion& other)
{
    x += other.x;
    y += other.y;
    z += other.z;
    w += other.w;

    return *this;
}

Quaternion Quaternion::operator-(const Quaternion& other) const
{
    return Quaternion(x - other.x, y - other.y, z - other.z, w - other.w);
}

const Quaternion& Quaternion::operator-=(const Quaternion& other)
{
    x -= other.x;
    y -= other.y;
    z -= other.z;
    w -= other.w;

    return *this;
}

Quaternion Quaternion::operator*(float multiplier) const
{
    return Quaternion(x * multiplier, y * multiplier, z * multiplier, w * multiplier);
}

const Quaternion& Quaternion::operator*=(float multiplier)
{
    x *= multiplier;
    y *= multiplier;
    z *= multiplier;
    w *= multiplier;

    return *this;
}

Quaternion Quaternion::operator/(float divider) const
{
    return Quaternion(x / divider, y / divider, z / divider, w / divider);
}

const Quaternion& Quaternion::operator/=(float divider)
{
    x /= divider;
    y /= divider;
    z /= divider;
    w /= divider;

    return *this;
}

Quaternion Quaternion::normalize(const Quaternion& q)
{
    return q / Quaternion::length(q);
}

Quaternion Quaternion::conjugate(const Quaternion& q)
{
    return Quaternion(-q.x, -q.y, -q.z, q.w);
}

Quaternion Quaternion::inverse(const Quaternion& q)
{
    return Quaternion::conjugate(q) / Quaternion::norm(q);
}

float Quaternion::norm(const Quaternion& q)
{
    return q.x * q.x + q.y * q.y + q.z * q.z + q.w * q.w;
}

float Quaternion::length(const Quaternion& q)
{
    return sqrtf(Quaternion::norm(q));
}

float Quaternion::dot(const Quaternion& one,
                      const Quaternion& another)
{
    return one.x * another.x + one.y * another.y + one.z * another.z + one.w * another.w;
}

Quaternion Quaternion::cross(const Quaternion& one,
                             const Quaternion& another)
{
    Vector one_v(one.x, one.y, one.z);
    Vector another_v(another.x, another.y, another.z);

    Vector result_v = Vector::cross(one_v, another_v) + one_v * another.w + another_v * one.w;

    Quaternion result;
    result.x = result_v.x;
    result.y = result_v.y;
    result.z = result_v.z;
    result.w = one.w * another.w - Vector::dot(one_v, another_v);

    return result;
}

Quaternion Quaternion::cross(const Quaternion& one,
                             const Vector& another)
{
    Vector one_v(one.x, one.y, one.z);

    Vector result_v = Vector::cross(one_v, another) + another * one.w;
    Quaternion result;
    result.x = result_v.x;
    result.y = result_v.y;
    result.z = result_v.z;
    result.w = -Vector::dot(one_v, another);

    return result;
}

Vector Quaternion::rotate(const Vector& v,
                          const Quaternion& q,
                          bool inverse)
{
    auto q_inversed = Quaternion::inverse(q);

    Quaternion result;
    if (inverse)
        result = Quaternion::cross(Quaternion::cross(q_inversed, v), q);
    else
        result = Quaternion::cross(Quaternion::cross(q, v), q_inversed);

    return Vector(result.x, result.y, result.z);
}


vector<unsigned char> Quaternion::serialize(Serializable::Type type) const
{
    vector<unsigned char> data(size(type), 0x00);

    serialize(data.data(), type);

    return data;
}

void Quaternion::serialize(unsigned char* pointer,
                           Serializable::Type type) const
{
    float* float_pointer;

    switch (type)
    {
        case Type::MinimalSize:
        case Type::WithAlignment:
            float_pointer = reinterpret_cast<float*>(pointer);
            *float_pointer++ = x;
            *float_pointer++ = y;
            *float_pointer++ = z;
            *float_pointer = w;
            break;

        default:
            throw logic_error("Unsupported serialization type.");
    }
}

void Quaternion::deserialize(const vector<unsigned char>& data,
                             Serializable::Type type)
{
    if (data.size() != size(type))
        throw logic_error("Size of the vector with data does not comply with requirements for Quaternion deserialization.");

    deserialize(data.data(), type);
}

void Quaternion::deserialize(const unsigned char* pointer,
                             Serializable::Type type)
{
const float* float_pointer;

    switch(type)
    {
        case Type::MinimalSize:
        case Type::WithAlignment:
            float_pointer = reinterpret_cast<const float*>(pointer);
            x = *float_pointer++;
            y = *float_pointer++;
            z = *float_pointer++;
            w = *float_pointer;
            break;

        default:
            throw logic_error("Unsupported serialization type." );
    }
}

size_t Quaternion::size(Serializable::Type type) const
{
    switch(type)
    {
        case Type::MinimalSize:
        case Type::WithAlignment:
            return sizeof(float) * 4;

        default:
            throw logic_error("Unsupported serialization type.");
    }
}
