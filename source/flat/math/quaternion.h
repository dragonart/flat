#pragma once


#include "utility/serializable.h"


namespace flat
{
    struct Vector;


    struct Quaternion : public Serializable
    {
        float x;
        float y;
        float z;
        float w;


        Quaternion(float x = 0.0f,
                   float y = 0.0f,
                   float z = 0.0f,
                   float w = 0.0f);
        Quaternion(const Vector& axis,
                   float angle);
        Quaternion(const Quaternion& other);

        const Quaternion& operator=(const Quaternion& other);

        bool operator==(const Quaternion& other) const;
        bool operator!=(const Quaternion& other) const;

        Quaternion operator+(const Quaternion& other) const;
        const Quaternion& operator+=(const Quaternion& other);

        Quaternion operator-(const Quaternion& other) const;
        const Quaternion& operator-=(const Quaternion& other);

        Quaternion operator*(float multiplier) const;
        const Quaternion& operator*=(float multiplier);

        Quaternion operator/(float divider) const;
        const Quaternion& operator/=(float divider);


        static Quaternion normalize(const Quaternion& q);
        static Quaternion conjugate(const Quaternion& q);
        static Quaternion inverse(const Quaternion& q);

        static float norm(const Quaternion& q);
        static float length(const Quaternion& q);

        static float dot(const Quaternion& one,
                         const Quaternion& another);
        static Quaternion cross(const Quaternion& one,
                                const Quaternion& another);
        static Quaternion cross(const Quaternion& one,
                                const Vector& another);

        static Vector rotate(const Vector& v,
                                 const Quaternion& q,
                                 bool inverse = false);


        vector<unsigned char> serialize(Type type) const override;
        void serialize(unsigned char* pointer,
                               Type type) const override;
        void deserialize(const vector<unsigned char>& data,
                                 Type type) override;
        void deserialize(const unsigned char* pointer,
                                 Type type) override;
        size_t size(Type type) const override;
    };
}