#include <math.h>
#include <stdexcept>

#include "vector.h"


using namespace flat;

using std::logic_error;


Vector::Vector(float x,
               float y,
               float z) :
        x(x),
        y(y),
        z(z)
{ }

Vector::Vector(const Vector& other) :
        x(other.x),
        y(other.y),
        z(other.z)
{ }


const Vector& Vector::operator=(const Vector& other)
{
    x = other.x;
    y = other.y;
    z = other.z;

    return *this;
}


bool Vector::operator==(const Vector& other) const
{
    return (x == other.x) && (y == other.y) && (z == other.z);
}

bool Vector::operator!=(const Vector& other) const
{
    return (x != other.x) || (y != other.y) || (z != other.z);
}


Vector Vector::operator+(const Vector& other) const
{
    return Vector(x + other.x, y + other.y, z + other.z);
}

const Vector& Vector::operator+=(const Vector& other)
{
    x += other.x;
    y += other.y;
    z += other.z;

    return *this;
}

Vector Vector::operator-(const Vector& other) const
{
    return Vector(x - other.x, y - other.y, z - other.z);
}

const Vector& Vector::operator-=(const Vector& other)
{
    x -= other.x;
    y -= other.y;
    z -= other.z;

    return *this;
}

Vector Vector::operator*(float multiplier) const
{
    return Vector(x * multiplier, y * multiplier, z * multiplier);
}

const Vector& Vector::operator*=(float multiplier)
{
    x *= multiplier;
    y *= multiplier;
    z *= multiplier;

    return *this;
}

Vector Vector::operator/(float divider) const
{
    return Vector(x / divider, y / divider, z / divider);
}

const Vector& Vector::operator/=(float divider)
{
    x /= divider;
    y /= divider;
    z /= divider;

    return *this;
}


Vector Vector::normalize(const Vector& v)
{
    return v / Vector::length(v);
}

Vector Vector::conjugate(const Vector& v)
{
    return Vector(-v.x, -v.y, -v.z);
}

Vector Vector::inverse(const Vector& v)
{
    return Vector::conjugate(v) / Vector::length(v);
}

float Vector::norm(const Vector& v)
{
    return v.x * v.x + v.y * v.y + v.z * v.z;
}

float Vector::length(const Vector& v)
{
    return sqrtf(Vector::norm(v));
}

float Vector::dot(const Vector& one,
                  const Vector& another)
{
    return one.x * another.x + one.y * another.y + one.z * another.z;
}

Vector Vector::cross(const Vector& one,
                     const Vector& another)
{
    Vector result;

    result.x = one.y * another.z - one.z * another.y;
    result.y = one.z * another.x - one.x * another.z;
    result.z = one.x * another.y - one.y * another.x;

    return result;
}

vector<unsigned char> Vector::serialize(Serializable::Type type) const
{
    vector<unsigned char> data(size(type), 0x00);

    serialize(data.data(), type);

    return data;
}

void Vector::serialize(unsigned char* pointer,
                       Type type) const
{
    auto float_pointer = reinterpret_cast<float*>(pointer);

    switch (type)
    {
        case Type::MinimalSize:
        case Type::WithAlignment:
            *float_pointer++ = x;
            *float_pointer++ = y;
            *float_pointer = z;
            break;

        default:
            throw logic_error("Unsupported serialization type.");
    }
}

void Vector::deserialize(const vector<unsigned char>& data,
                         Serializable::Type type)
{
    if (data.size() != size(type))
        throw logic_error("Size of the vector with data does not comply with requirements for Vector deserialization.");

    deserialize(data.data(), type);
}

void Vector::deserialize(const unsigned char* pointer,
                         Type type)
{
    auto float_pointer = reinterpret_cast<const float*>(pointer);

    switch(type)
    {
        case Type::MinimalSize:
        case Type::WithAlignment:
            x = *float_pointer++;
            y = *float_pointer++;
            z = *float_pointer;
            break;

        default:
            throw logic_error("Unsupported serialization type.");
    }
}

size_t Vector::size(Serializable::Type type) const
{
    switch (type)
    {
        case Type::MinimalSize:
            return sizeof(float) * 3;
        case Type::WithAlignment:
            return sizeof(float) * 4;

        default:
            throw logic_error("Unsupported serialization type.");
    }
}
