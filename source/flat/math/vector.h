#pragma once


#include "utility/serializable.h"


namespace flat
{
    class Vector : public Serializable
    {
    public:
        float x;
        float y;
        float z;


    public:
        Vector(float x = 0.0f,
               float y = 0.0f,
               float z = 0.0f);
        Vector(const Vector& other);

        const Vector& operator=(const Vector& other);

        bool operator==(const Vector& other) const;
        bool operator!=(const Vector& other) const;

        Vector operator+(const Vector& other) const;
        const Vector& operator+=(const Vector& other);

        Vector operator-(const Vector& other) const;
        const Vector& operator-=(const Vector& other);

        Vector operator*(float multiplier) const;
        const Vector& operator*=(float multiplier);

        Vector operator/(float divider) const;
        const Vector& operator/=(float divider);


        static Vector normalize(const Vector& v);
        static Vector conjugate(const Vector& v);
        static Vector inverse(const Vector& v);

        static float norm(const Vector& v);
        static float length(const Vector& v);

        static float dot(const Vector& one,
                         const Vector& another);
        static Vector cross(const Vector& one,
                            const Vector& another);


    public:
        virtual vector<unsigned char> serialize(Type type) const override;
        virtual void serialize(unsigned char* pointer,
                               Type type) const override;
        virtual void deserialize(const vector<unsigned char>& data,
                                 Type type) override;
        virtual void deserialize(const unsigned char* pointer,
                                 Type type) override;
        virtual size_t size(Type type) const override;

    };
}