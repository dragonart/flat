#include <iostream>


#define GLEW_STATIC

#include <GL/glew.h>

#include "screen.h"


using namespace flat;

using std::cout;
using std::endl;


void Screen::initialize(Size size)
{
    _frame.setSize(size);

    float vertices[] =
            {
                    -1.0f, -1.0f, 0.0f, 1.0f,
                    1.0f, -1.0f, 1.0f, 1.0f,
                    -1.0f, 1.0f, 0.0f, 0.0f,
                    1.0f, 1.0f, 1.0f, 0.0f
            };

    glGenVertexArrays(1, &_vertex_array_id);
    glBindVertexArray(_vertex_array_id);

    glGenBuffers(1, &_vertex_buffer_id);

    glBindBuffer(GL_ARRAY_BUFFER, _vertex_buffer_id);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glGenBuffers(1, &_element_buffer_id);

    unsigned int elements[] =
            {
                    0, 1, 2,
                    2, 1, 3
            };

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _element_buffer_id);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elements), elements, GL_STATIC_DRAW);


    const char* vertex_source =
            "#version 330 core\n" \
            "in vec2 position;" \
            "in vec2 uv_;" \
            "out vec2 uv;" \
            "void main()" \
            "{" \
            "  uv = uv_;" \
            "  gl_Position = vec4(position, 0.0, 1.0);" \
            "}";
    const char* fragment_source =
            "#version 330 core\n" \
            "in vec2 uv;" \
            "out vec4 color;" \
            "uniform sampler2D tex;" \
            "void main()" \
            "{" \
            "  color = texture(tex, uv);" \
            "}";


    _vertex_shader_id = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(_vertex_shader_id, 1, &vertex_source, nullptr);
    glCompileShader(_vertex_shader_id);

    GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment_shader, 1, &fragment_source, nullptr);
    glCompileShader(fragment_shader);

    _shader_program_id = glCreateProgram();
    glAttachShader(_shader_program_id, _vertex_shader_id);
    glAttachShader(_shader_program_id, fragment_shader);
    glBindFragDataLocation(_shader_program_id, 0, "color");
    glLinkProgram(_shader_program_id);
    glUseProgram(_shader_program_id);

    GLint position_attribute = glGetAttribLocation(_shader_program_id, "position");
    glEnableVertexAttribArray(position_attribute);
    glVertexAttribPointer(position_attribute, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), nullptr);
    GLint uv_attribute = glGetAttribLocation(_shader_program_id, "uv_");
    glEnableVertexAttribArray(uv_attribute);
    glVertexAttribPointer(uv_attribute, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), reinterpret_cast<void*>(2 * sizeof(float)));


    glGenTextures(1, &_screen_id);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size.width, size.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, reinterpret_cast<unsigned char*>(_frame.getPixels().data()));


    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);


    cout << "Screen initialization succeeded." << endl;
}

void Screen::release()
{
    glDeleteTextures(1, &_screen_id);

    glDeleteProgram(_shader_program_id);
    glDeleteShader(_fragment_shader_id);
    glDeleteShader(_vertex_shader_id);

    glDeleteBuffers(1, &_element_buffer_id);
    glDeleteBuffers(1, &_vertex_buffer_id);

    glDeleteVertexArrays(1, &_vertex_array_id);

    cout << "Screen release succeeded." << endl;
}

void Screen::render() const
{
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, _frame.getSize().width, _frame.getSize().height, 0, GL_RGBA, GL_UNSIGNED_BYTE, _frame.getPixels().data());

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}

const Frame& Screen::getFrame() const
{
    return _frame;
}

Frame& Screen::getFrame()
{
    return _frame;
}
