#pragma once


#include <string>

#include "utility/serializable.h"


using std::string;
using std::stringstream;


namespace flat
{
    struct Color : public Serializable
    {
    public:
        unsigned char red; // TODO: protect fields
        unsigned char green;
        unsigned char blue;
        unsigned char alpha;


    public:
        Color(unsigned char red = 0x00,
              unsigned char green = 0x00,
              unsigned char blue = 0x00,
              unsigned char alpha = 0xff);
        virtual ~Color();

        bool operator==(const Color& other) const;
        bool operator!=(const Color& other) const;


        virtual vector<unsigned char> serialize(Type type) const override;
        virtual void serialize(unsigned char* pointer,
                               Type type) const override;
        virtual void deserialize(const vector<unsigned char>& data,
                                 Type type) override;
        virtual void deserialize(const unsigned char* pointer,
                                 Type type) override;
        virtual size_t size(Type type) const override;

        string toString() const;
    };
}