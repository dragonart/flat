#pragma once


#include <vector>

#include "color.h"
#include "utility/size.h"


using std::vector;


namespace flat
{
    class Frame
    {
    public:
        Frame(Size size = {0, 0});

        Size getSize() const;
        void setSize(Size new_size);

        const vector<Color>& getPixels() const;
        vector<Color>& getPixels();


    private:
        Size _size;

        vector<Color> _pixels;
    };
}