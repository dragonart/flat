#include "frame.h"


using namespace flat;


Frame::Frame(Size size) :
        _size(size),
        _pixels(size.width * size.height)
{ }


Size Frame::getSize() const
{
    return _size;
}

void Frame::setSize(Size new_size)
{
    _size = new_size;

    _pixels.clear();
    _pixels.resize(_size.width * _size.height);
}

const vector<Color>& Frame::getPixels() const
{
    return _pixels;
}

vector<Color>& Frame::getPixels()
{
    return _pixels;
}
