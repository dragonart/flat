#pragma once


#include "frame.h"


namespace flat
{
    class Screen
    {
    public:
        void initialize(Size size);
        void release();

        void render() const;

        const Frame& getFrame() const;
        Frame& getFrame();


    private:
        Frame _frame;
        unsigned int _screen_id;

        unsigned int _vertex_array_id;
        unsigned int _vertex_buffer_id;
        unsigned int _element_buffer_id;

        unsigned int _vertex_shader_id;
        unsigned int _fragment_shader_id;
        unsigned int _shader_program_id;
    };
}