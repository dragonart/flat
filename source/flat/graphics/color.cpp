#include <stdexcept>
#include <sstream>
#include <iomanip>

#include "color.h"


using namespace flat;

using std::logic_error;
using std::stringstream;
using std::internal;
using std::setfill;
using std::hex;
using std::setw;


Color::Color(unsigned char red,
             unsigned char green,
             unsigned char blue,
             unsigned char alpha) :
        red(red),
        green(green),
        blue(blue),
        alpha(alpha)
{ }

Color::~Color()
{ }

bool Color::operator==(const Color& other) const
{
    return (red == other.red) && (green == other.green) && (blue == other.blue) && (alpha == other.alpha);
}

bool Color::operator!=(const Color& other) const
{
    return (red != other.red) || (green != other.green) || (blue != other.blue) || (alpha != other.alpha);
}


vector<unsigned char> Color::serialize(Serializable::Type type) const
{
    vector<unsigned char> data(size(type), 0x00);
    auto pointer = data.data();

    switch (type)
    {
        case Type::MinimalSize:
        case Type::WithAlignment:
            serialize(pointer, type);
            break;

        default:
            throw logic_error("Unsupported serialization type.");
    }

    return data;
}

void Color::serialize(unsigned char* pointer,
                      Serializable::Type type) const
{
    switch (type)
    {
        case Type::MinimalSize:
        case Type::WithAlignment:
            *pointer++ = red;
            *pointer++ = green;
            *pointer++ = blue;
            *pointer = alpha;
            break;

        default:
            throw logic_error("Unsupported serialization type.");
    }
}

void Color::deserialize(const vector<unsigned char>& data,
                        Serializable::Type type)
{
    if (data.size() != size(type))
        throw logic_error("Size of the vector with data does not comply with requirements for Color deserialization.");

    auto pointer = data.data();
    deserialize(pointer, type);
}

void Color::deserialize(const unsigned char* pointer,
                        Serializable::Type type)
{
    switch (type)
    {
        case Type::MinimalSize:
        case Type::WithAlignment:
            red = *pointer++;
            green = *pointer++;
            blue = *pointer++;
            alpha = *pointer;
            break;

        default:
            throw logic_error("Unsupported serialization type.");
    }
}

size_t Color::size(Serializable::Type type) const
{
    switch (type)
    {
        case Type::MinimalSize:
        case Type::WithAlignment:
            return 4;

        default:
            throw logic_error("Unsupported serialization type.");
    }
}

string Color::toString() const
{
    stringstream stream;

    stream << internal << setfill('0');
    stream << hex << setw(2) << static_cast<unsigned short>(red) << " ";
    stream << hex << setw(2) << static_cast<unsigned short>(green) << " ";
    stream << hex << setw(2) << static_cast<unsigned short>(blue) << " ";
    stream << hex << setw(2) << static_cast<unsigned short>(alpha);

    return stream.str();
}
