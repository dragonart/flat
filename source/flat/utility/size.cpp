#include "size.h"


using namespace flat;


Size::Size(unsigned int new_width,
           unsigned int new_height) :
        width(new_width),
        height(new_height)
{ }


bool Size::operator==(const Size& other)
{
    return (width == other.width) && (height == other.height);
}

bool Size::operator!=(const Size& other)
{
    return (width != other.width) || (height != other.height);
}