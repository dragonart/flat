#pragma once


#include <vector>


using std::vector;
using std::size_t;


namespace flat
{
    class Serializable
    {
    public:
        enum class Type : unsigned char
        {
            Unknown,
            MinimalSize,
            WithAlignment
        };


    public:
        virtual ~Serializable()
        { }

        virtual vector<unsigned char> serialize(Type type = Type::MinimalSize) const = 0;
        virtual void serialize(unsigned char* pointer,
                               Type type = Type::MinimalSize) const = 0;

        virtual void deserialize(const vector<unsigned char>& data,
                                 Type type = Type::MinimalSize) = 0;
        virtual void deserialize(const unsigned char* pointer,
                                 Type type = Type::MinimalSize) = 0;

        virtual size_t size(Type type = Type::MinimalSize) const = 0;
    };
}