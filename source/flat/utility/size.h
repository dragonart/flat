#pragma once


namespace flat
{
    struct Size
    {
        unsigned int width;
        unsigned int height;


        Size(unsigned int new_width = 0,
             unsigned int new_height = 0);

        bool operator==(const Size& other);
        bool operator!=(const Size& other);
    };
}