#pragma once


namespace flat
{
    class Noncopyable
    {
    protected:
        Noncopyable()
        { }

        ~Noncopyable()
        { }


    private:
        Noncopyable(const Noncopyable&);
        Noncopyable& operator=(const Noncopyable&);
    };
}