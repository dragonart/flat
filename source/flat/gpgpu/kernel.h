#pragma once


#include <CL/cl.h>


namespace flat
{
    class Kernel
    {
        friend class Gpgpu;


    public:
        Kernel();

        Kernel(const Kernel& other) = delete;
        const Kernel& operator=(const Kernel& other) = delete;


    private:
        cl_kernel _kernel;
        unsigned int _last_argument_index;
    };
}