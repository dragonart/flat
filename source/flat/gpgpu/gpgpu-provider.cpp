#include "gpgpu-provider.h"


using namespace flat;

using std::make_shared;


shared_ptr<Gpgpu> GpgpuProvider::gpgpu()
{
    static auto gpgpu_ptr = make_shared<Gpgpu>();
    return gpgpu_ptr;
}
