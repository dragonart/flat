#include <iostream>

#include "gpgpu.h"


using namespace flat;

using std::cout;
using std::endl;
using std::to_string;
using std::runtime_error;
using std::logic_error;
using std::make_shared;


void Gpgpu::initialize()
{
    cl_int error;
    cl_uint platforms_number = 0;

    error = clGetPlatformIDs(1, nullptr, &platforms_number);
    checkError(error, "GPGPU: clGetPlatformIDs()");

    cout << "GPGPU platforms found: " << platforms_number << endl;

    _platform_ids.resize(platforms_number);
    error = clGetPlatformIDs(platforms_number, _platform_ids.data(), nullptr);
    checkError(error, "GPGPU: clGetPlatformIDs()");

    for (auto i = 0; i < platforms_number; ++i)
        cout << "    " << i + 1 << ". " << getPlatformName(_platform_ids[i]) << endl;


    cl_uint devices_number = 0;

    error = clGetDeviceIDs(_platform_ids[0], CL_DEVICE_TYPE_ALL, 0, nullptr, &devices_number);
    checkError(error, "GPGPU: clGetDeviceIDs()");


    cout << "GPGPU devices found: " << devices_number << endl;

    _device_ids.resize(devices_number);
    error = clGetDeviceIDs(_platform_ids[0], CL_DEVICE_TYPE_ALL, devices_number, _device_ids.data(), nullptr);
    checkError(error, "GPGPU: clGetDeviceIDs()");


    for (auto i = 0; i < devices_number; ++i)
        cout << "    " << i + 1 << ". " << getDeviceName(_device_ids[i]) << endl;


    const cl_context_properties context_properties[] =
            {
                    CL_CONTEXT_PLATFORM,
                    reinterpret_cast<cl_context_properties>(_platform_ids[0]),
                    0, 0
            };

    error = CL_SUCCESS;
    _context = clCreateContext(context_properties, _device_ids.size(), _device_ids.data(), nullptr, nullptr, &error);
    checkError(error, "GPGPU: clCreateContext()");

    cout << "GPGPU context created." << endl;


    _memory_size = 0;


    _queue = clCreateCommandQueue(_context, _device_ids[0], 0, &error);
    checkError(error, "GPGPU: clCreateCommandQueue()");

    cout << "GPGPU initialization succeeded." << endl;
}

void Gpgpu::release()
{
    cl_int error;

    if (_memory_size != 0)
    {
        error = clReleaseMemObject(_memory);
        checkError(error, "GPGPU: clReleaseMemObject(memory)");
    }

    error = clReleaseCommandQueue(_queue);
    checkError(error, "GPGPU: clReleaseCommandQueue()");

    for (auto& kernel : _kernels)
    {
        error = clReleaseKernel(kernel->_kernel);
        checkError(error, "GPGPU: clReleaseKernel()");
    }

    for (auto& program : _programs)
    {
        error = clReleaseProgram(program->_program);
        checkError(error, "GPGPU: clReleaseProgram()");
    }

    error = clReleaseContext(_context);
    checkError(error, "GPGPU: clReleaseContext()");

    cout << "GPGPU release succeeded." << endl;
}

void Gpgpu::reserve(size_t size)
{
    if (_memory_size != 0)
        throw logic_error("GPGPU: second-time reserve() is not allowed just now.");

    cl_int error = CL_SUCCESS;

    _memory = clCreateBuffer(_context, CL_MEM_READ_WRITE, size, nullptr, &error);
    checkError(error, "GPGPU: clCreateBuffer()");

    _memory_size = size;
    cout << "GPGPU memory reserved: " << size << " bytes." << endl;
}

void Gpgpu::checkError(cl_int& code,
                       string message)
{
    if (code != CL_SUCCESS)
        throw runtime_error(message + " failed with error code " + to_string(code) + ".");
}

string Gpgpu::getPlatformName(cl_platform_id id)
{
    size_t size = CL_SUCCESS;

    clGetPlatformInfo(id, CL_PLATFORM_NAME, 0, nullptr, &size);
    string name;
    name.resize(size);
    clGetPlatformInfo(id, CL_PLATFORM_NAME, size, const_cast<char*>(name.data()), nullptr);

    clGetPlatformInfo(id, CL_PLATFORM_VENDOR, 0, nullptr, &size);
    string vendor;
    vendor.resize(size);
    clGetPlatformInfo(id, CL_PLATFORM_VENDOR, size, const_cast<char*>(vendor.data()), nullptr);

    return name + " [ " + vendor + "]";
}

string Gpgpu::getDeviceName(cl_device_id id)
{
    size_t size = CL_SUCCESS;

    clGetDeviceInfo(id, CL_DEVICE_NAME, 0, nullptr, &size);
    string name;
    name.resize(size);
    clGetDeviceInfo(id, CL_DEVICE_NAME, size, const_cast<char*>(name.data()), nullptr);

    clGetDeviceInfo(id, CL_DEVICE_VENDOR, 0, nullptr, &size);
    string vendor;
    vendor.resize(size);
    clGetDeviceInfo(id, CL_DEVICE_VENDOR, size, const_cast<char*>(vendor.data()), nullptr);

    return name + " [ " + vendor + "]";
}

shared_ptr<Program> Gpgpu::createProgram(const string& source)
{
    size_t lengths[1] = {source.size()};
    const char* sources[1] = {source.data()};

    cl_int error = CL_SUCCESS;

    cl_program program = clCreateProgramWithSource(_context, 1, sources, lengths, &error);
    checkError(error, "GPGPU: clCreateProgramWithSource()");

    error = clBuildProgram(program, _device_ids.size(), _device_ids.data(), "-DCL_LOG_ERRORS=stdout", nullptr, nullptr);
    if (error != CL_SUCCESS)
    {
        cl_build_status build_status;
        clGetProgramBuildInfo(program, _device_ids[0], CL_PROGRAM_BUILD_STATUS, sizeof(cl_build_status), &build_status, nullptr);

        size_t log_size = 0;
        clGetProgramBuildInfo(program, _device_ids[0], CL_PROGRAM_BUILD_LOG, 0, nullptr, &log_size);

        string program_build_log;
        program_build_log.resize(log_size + 1);
        clGetProgramBuildInfo(program, _device_ids[0], CL_PROGRAM_BUILD_LOG, log_size + 1, static_cast<void*>(&program_build_log[0]), nullptr);

        cout << "GPGPU program build failed:" << endl;
        cout << "  Error: " << error << endl;
        cout << "  Build status: " << build_status << endl;
        cout << "  Build log: " << program_build_log << endl;
    }

    _programs.push_back(make_shared<Program>());
    _programs.back()->_program = program;

    return _programs.back();
}

shared_ptr<Kernel> Gpgpu::createKernel(shared_ptr<Program> program,
                                       const string& name)
{
    cl_int error = CL_SUCCESS;

    cl_kernel kernel = clCreateKernel(program->_program, name.data(), &error);
    checkError(error, "GPGPU: clCreateKernel()");

    _kernels.push_back(make_shared<Kernel>());
    _kernels.back()->_kernel = kernel;
    _kernels.back()->_last_argument_index = -1;

    return _kernels.back();
}

shared_ptr<Buffer> Gpgpu::createBuffer(void* data,
                                       size_t size)
{
    cl_int error = CL_SUCCESS;

    size_t start = 0;
    if (!_buffers.empty())
        start = _buffers.back()->_start + _buffers.back()->_size;

    if (start + size > _memory_size)
        throw logic_error("GPGPU: createBuffer() failed: there is no enough memory reserved.");

    _buffers.push_back(make_shared<Buffer>(start, size));

    if (data != nullptr)
        writeBuffer(_buffers.back(), data);

    return _buffers.back();
}

void Gpgpu::releaseBuffer(shared_ptr<Buffer>& buffer)
{
    // do nothing
    buffer = nullptr;
}

void Gpgpu::readBuffer(const shared_ptr<Buffer>& buffer,
                       void* data)
{
    cl_int error = clEnqueueReadBuffer(_queue, _memory, CL_TRUE, buffer->_start, buffer->_size, data, 0, nullptr, nullptr);
    checkError(error, "GPGPU: clEnqueueReadBuffer()");
}

void Gpgpu::writeBuffer(const shared_ptr<Buffer>& buffer,
                        const void* data)
{
    cl_int error = clEnqueueWriteBuffer(_queue, _memory, CL_TRUE, buffer->_start, buffer->_size, data, 0, nullptr, nullptr);
    checkError(error, "GPGPU: clEnqueueWriteBuffer()");
}

void Gpgpu::writeBuffer(const shared_ptr<Buffer>& buffer,
                        const void* data,
                        size_t size,
                        size_t offset)
{
    cl_int error = clEnqueueWriteBuffer(_queue, _memory, CL_TRUE, buffer->_start + offset, size, data, 0, nullptr, nullptr);
    checkError(error, "GPGPU: clEnqueueWriteBuffer()");
}

void Gpgpu::setKernelArgument(shared_ptr<Kernel> kernel,
                              unsigned int argument_index,
                              void* data,
                              size_t size)
{
    cl_int error = clSetKernelArg(kernel->_kernel, argument_index, size, data);
    checkError(error, "GPGPU: clSetKernelArg()");

    kernel->_last_argument_index = argument_index;
}

void Gpgpu::setKernelArgument(shared_ptr<Kernel> kernel,
                              unsigned int argument_index,
                              const shared_ptr<Buffer>& buffer)
{
    setKernelArgument(kernel, argument_index, buffer.get(), sizeof(Buffer));
}

void Gpgpu::runKernel(shared_ptr<Kernel> kernel,
                      unsigned char dimension,
                      size_t* size)
{
    if (_memory_size != 0) if (kernel->_last_argument_index != -1)
    {
        setKernelArgument(kernel, kernel->_last_argument_index + 1, &_memory, sizeof(cl_mem));
        kernel->_last_argument_index--;
    }

    std::size_t offset[3] = {0};
    cl_int error = clEnqueueNDRangeKernel(_queue, kernel->_kernel, dimension, offset, size, nullptr, 0, nullptr, nullptr);
    checkError(error, "GPGPU: clEnqueueNDRangeKernel()");
}