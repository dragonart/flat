#pragma once


#include <vector>
#include <memory>

#include <CL/cl.h>

#include "buffer.h"
#include "program.h"
#include "kernel.h"


using std::vector;
using std::string;
using std::shared_ptr;


namespace flat
{
    class Gpgpu
    {
    public:
        void initialize();
        void release();

        void reserve(size_t size);
        shared_ptr<Program> createProgram(const string& source);
        shared_ptr<Kernel> createKernel(shared_ptr<Program> program,
                                        const string& name);

        shared_ptr<Buffer> createBuffer(void* data,
                                        size_t size);
        void releaseBuffer(shared_ptr<Buffer>& buffer);

        void readBuffer(const shared_ptr<Buffer>& buffer,
                        void* data);
        void writeBuffer(const shared_ptr<Buffer>& buffer,
                         const void* data);
        void writeBuffer(const shared_ptr<Buffer>& buffer,
                         const void* data,
                         size_t size,
                         size_t offset = 0);

        void setKernelArgument(shared_ptr<Kernel> kernel,
                               unsigned int argument_index,
                               void* data,
                               size_t size);
        void setKernelArgument(shared_ptr<Kernel> kernel,
                               unsigned int argument_index,
                               const shared_ptr<Buffer>& buffer);
        void runKernel(shared_ptr<Kernel> kernel,
                       unsigned char dimension,
                       size_t* size);


    private:
        vector<cl_platform_id> _platform_ids;
        vector<cl_device_id> _device_ids;
        cl_context _context;
        cl_command_queue _queue;
        vector<shared_ptr<Program>> _programs;
        vector<shared_ptr<Kernel>> _kernels;
        vector<shared_ptr<Buffer>> _buffers;

        cl_mem _memory;
        size_t _memory_size;


    private:
        static void checkError(cl_int& code,
                               string message);

        static string getPlatformName(cl_platform_id);
        static string getDeviceName(cl_device_id id);
    };
}