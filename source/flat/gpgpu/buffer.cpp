#include "buffer.h"


using namespace flat;


Buffer::Buffer()
{ }

Buffer::Buffer(size_t start,
               size_t size) :
        _start(start),
        _size(size)
{ }

size_t Buffer::getStart() const
{
    return _start;
}
