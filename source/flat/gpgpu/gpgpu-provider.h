#pragma once


#include "gpgpu.h"


namespace flat
{
    class GpgpuProvider
    {
    public:
        static shared_ptr<Gpgpu> gpgpu();
    };
}