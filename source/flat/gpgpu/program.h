#pragma once


#include <CL/cl.h>


namespace flat
{
    class Program
    {
        friend class Gpgpu;


    public:
        Program();

        Program(const Program& other) = delete;
        const Program& operator=(const Program& other) = delete;


    private:
        cl_program _program;
    };
}