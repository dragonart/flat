#pragma once


#include <CL/cl.h>


namespace flat
{
    class Buffer
    {
        friend class Gpgpu;


    public:
        Buffer();
        Buffer(size_t start,
               size_t size);

        Buffer(const Buffer& other) = delete;
        const Buffer& operator=(const Buffer& other) = delete;

        size_t getStart() const;


    private:
        size_t _start;
        size_t _size;
    };
}